Pic4Carto.js
============

[![build status](https://framagit.org/Pic4Carto/Pic4Carto.js/badges/master/build.svg)](https://framagit.org/Pic4Carto/Pic4Carto.js/commits/master)

Read-me
-------

Pic4Carto.js is a library for efficient retrieval of geolocated, free-licensed pictures. Its goal is to make easier to reuse pictures from several providers (Panoramax, Flickr, Mapillary, KartaView, Wikimedia Commons...). Using a simple API, you can retrieve all the pictures freely available over a given area. See the [list of available providers](https://framagit.org/Pic4Carto/Pic4Carto.js/blob/master/doc/Fetchers.md).

For the HTTP API version of Pic4Carto, please see [the P4CaaS project](https://framagit.org/PanierAvide/P4CaaS).

[![Help making this possible](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/PanierAvide/donate)


Install
-------

Pic4Carto.js can be installed following one of these methods.

### Using npm
```sh
npm install pic4carto
```
Then, access it in JS:
```js
import * as P4C from "pic4carto";
```

### Using bundle version
You can use versions offered by various CDN, for example:
```html
<script src="https://cdn.jsdelivr.net/npm/pic4carto"></script>
<script>
	var picManager = new P4C.PicturesManager();
</script>
```


Usage
-----

Here are some rapid examples of how to use Pic4Carto.js. If you want working examples, see HTML examples in _dist/_ folder. For complete documentation, check the [API details](https://framagit.org/Pic4Carto/Pic4Carto.js/blob/master/doc/API.md).

### Get pictures in a given area
```js
var picManager = new P4C.PicturesManager();

//Call for pictures download
picManager.startPicsRetrieval(new P4C.LatLngBounds(new P4C.LatLng(48.1075, -1.6860), new P4C.LatLng(48.1156, -1.6739)))
.then(function(pictures) { //Called when promise resolves
	for(let pId=0; pId < pictures.length; pId++) {
		let currentPicture = pictures[pId];
		console.log(currentPicture.pictureUrl);
	}
});
```

### Get pictures within a circle
```js
var picManager = new P4C.PicturesManager();

//Call for pictures download
picManager.startPicsRetrievalAround(new P4C.LatLng(48.1075, -1.6860), 15) //15 meters around this point
.then(function(pictures) { //Called when promise resolves
	for(let pId=0; pId < pictures.length; pId++) {
		let currentPicture = pictures[pId];
		console.log(currentPicture.pictureUrl);
	}
});
```

### Show message when a fetcher has done or failed
```js
var picManager = new P4C.PicturesManager();

//Asynchronous events for watching fetchers progress
picManager
	.on("fetcherdone", (fetcherId) => {
		console.log("Fetcher "+fetcherId+" finished its pictures retrieval");
	})
	.on("fetcherfailed", (fetcherId) => {
		console.log("Fetcher "+fetcherId+" failed to retrieve pictures");
	});

//Filtering only recent pictures (since mid-2016)
picManager.startPicsRetrieval(new P4C.LatLngBounds(new P4C.LatLng(48.1075, -1.6860), new P4C.LatLng(48.1156, -1.6739)), { mindate: 1464775200000 })
.then((picsArray) => { //Called when promise resolves
	for(let pId=0; pId < picsArray.length; pId++) {
		let currentPicture = picsArray[pId];
		console.log(currentPicture.pictureUrl);
	}
});
```

### Get summary of pictures availability
```js
var picManager = new P4C.PicturesManager();

//Call for pictures summary download (around Rennes center)
picManager.startSummaryRetrieval(new P4C.LatLngBounds(new P4C.LatLng(48.1075, -1.6860), new P4C.LatLng(48.1156, -1.6739)))
.then(function(summary) { //Called when promise resolves
	console.log("Available pictures: "+((summary.approxAmount) ? "~" : "")+summary.amount+" | Last picture date: "+(new Date(summary.last).toDateString()));
});
```

### Search through statically served images
```js
var picManager = new P4C.PicturesManager({
	userDefinedFetchers: {
		ownfetch: {
			csv: "http://my.server.net/own/index.csv", // Mandatory
			name: "OwnFetcher", // Recommended
			homepage: "http://my.server.net/own/", // Optional
			logo: "http://my.server.net/own/logo.png", // Recommended
			bbox: new LatLngBounds(new LatLng(1.2, 48.7), new LatLng(1.3, 48.8)), // Recommended if you server pictures in a specific country/area
			license: "CC By-SA 3.0", // Optional, can be overriden in CSV index
			user: "Default user name" // Optional, can be overriden in CSV index
		}
	}
});

//then call picManager.startPicsRetrieval as shown above
```

### Get automatic detections from pictures in a given area
```js
var picManager = new P4C.PicturesManager();

//Call for pictures download
picManager.startDetectionsRetrieval(
	new P4C.LatLngBounds(new P4C.LatLng(48.1075, -1.6860), new P4C.LatLng(48.1156, -1.6739)),
	{ types: [ P4C.Detection.SIGN_ADVERT, P4C.Detection.SIGN_STORE ] }
)
.then(function(detections) { //Called when promise resolves
	for(let dId=0; dId < detections.length; dId++) {
		let currentDetection = detections[dId];
		console.log(currentDetection.type);
		console.log(currentDetection.date);
		console.log(currentDetection.coordinates);
	}
});
```


Build
-----

### Dependencies
* npm

### Compiling

If you want to build Pic4Carto.js by yourself, run the following commands:

```bash
npm install
npm run build
```
When this is done, Pic4Carto.js is ready in `build/` folder.


Tests
-----

A test suite is available, to see results run:

```bash
npm run test
```


Contributing
------------

Pic4Carto.js is an open-source project, and contributions are welcome. Here is how you can help:

* Let us know about any free-licensed pictures provider you want to see integrated in Pic4Carto.js.
* Report bugs or discuss new functionalities in the [Issues tracker](https://framagit.org/Pic4Carto/Pic4Carto.js/issues).
* Write code (and tests) and create a [merge request](https://framagit.org/Pic4Carto/Pic4Carto.js/merge_requests). As this project is using [Git Flow branching model](http://nvie.com/posts/a-successful-git-branching-model/), please create requests on __develop__ or __feature/*__ branches.


License
-------

Copyright 2016-2024 [Adrien PAVIE](http://pavie.info/)

See LICENSE for complete AGPL3 license. Pic4Carto.js is a fork based on [Pic4Carto](https://framagit.org/Pic4Carto/Pic4Carto) code basis.

Pic4Carto.js is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Pic4Carto.js is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Pic4Carto.js. If not, see <http://www.gnu.org/licenses/>.
