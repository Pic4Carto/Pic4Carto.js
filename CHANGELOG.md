# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [3.0.0] - 2024-08-14

### Added

- Panoramax fetcher
- This changelog file

### Changed

- Use of [JS Library Boilerplate](https://github.com/DevUnltd/js-library-boilerplate-basic) for building and testing instead of _Mocha & Browserify_.
- OpenStreetCam fetcher has been renamed and updated to be KartaView. This is a breaking change, as fetcher ID has changed.

## [2.1.15] - 2021-08-19

Changelog to be completed...

[Unreleased]: https://framagit.org/Pic4Carto/Pic4Carto.js/-/compare/v3.0.0...develop
[3.0.0]: https://framagit.org/Pic4Carto/Pic4Carto.js/-/compare/v2.1.15...v3.0.0
[2.1.15]: https://framagit.org/Pic4Carto/Pic4Carto.js/-/tree/v2.1.15
