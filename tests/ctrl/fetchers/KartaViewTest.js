/*
 * Test script for ctrl/fetchers/KartaView.js
 */


import LatLng from "../../../src/lib/model/LatLng";
import LatLngBounds from "../../../src/lib/model/LatLngBounds";
import KartaView from "../../../src/lib/ctrl/fetchers/KartaView";

const REQUEST_TIMEOUT = 30000;

describe("Constructor", function() {
	it("Constructor with valid parameters", () => {
		const m1 = new KartaView();
		
		//Check attributes
		expect(m1.name).toEqual("KartaView");
		expect(/https:\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/.test(m1.homepageUrl)).toBe(true);
		expect(/https:\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/.test(m1.logoUrl)).toBe(true);
	});
});

describe("requestPictures", function() {
	it("requestPictures with valid parameters", () => {
		const m1 = new KartaView();
		const bbox = new LatLngBounds(new LatLng(37.7642, -122.4604), new LatLng(37.7765, -122.4467));
		
		return m1.requestPictures(bbox)
		.then(pictures => {
			expect(pictures.length > 0).toBe(true);
			
			const pic1 = pictures[0];
			expect(pic1.author.length > 0).toBe(true);
			expect(pic1.license).toEqual("CC By-SA 4.0");
			expect(pic1.date > 0).toBe(true);
			expect(pic1.pictureUrl.startsWith("https://storage")).toBe(true);
			expect(pic1.detailsUrl.startsWith("https://kartaview.org/details/")).toBe(true);
			expect(pic1.provider).toEqual("KartaView");
			expect(bbox.contains(pic1.coordinates)).toBe(true);
			expect(pic1.direction === null || !isNaN(pic1.direction)).toBe(true);
			expect(pic1.osmTags.image).toEqual(pic1.pictureUrl);

		});
	}, REQUEST_TIMEOUT);

	it("requestPictures with valid parameters on empty area", () => {
		const m1 = new KartaView();
		const bbox = new LatLngBounds(new LatLng(-39.58687, -29.62719), new LatLng(-39.58498, -29.62503));

		return m1.requestPictures(bbox)
		.then(pictures => {
			expect(pictures.length).toEqual(0);
		});
	}, REQUEST_TIMEOUT);

	it("requestPictures with valid parameters and options defined", () => {
		const m1 = new KartaView();
		const bbox = new LatLngBounds(new LatLng(40.724, -73.989), new LatLng(40.725, -73.988));
		
		return m1.requestPictures(bbox, { mindate: 1467377425000 })
		.then(pictures => {
			expect(pictures.length > 0).toBe(true);
			
			const pic1 = pictures[0];
			expect(pic1.author.length > 0).toBe(true);
			expect(pic1.license).toEqual("CC By-SA 4.0");
			expect(pic1.date >= 1467377425000).toBe(true);
			expect(pic1.pictureUrl.startsWith("https://storage")).toBe(true);
			expect(pic1.detailsUrl.startsWith("https://kartaview.org/details/")).toBe(true);
			expect(pic1.provider).toEqual("KartaView");
			expect(bbox.contains(pic1.coordinates)).toBe(true);
			expect(pic1.direction === null || !isNaN(pic1.direction)).toBe(true);
		});
	}, REQUEST_TIMEOUT);
});

describe("requestSummary", function() {
	it("requestSummary with valid parameters", () => {
		const m1 = new KartaView();
		const bbox = new LatLngBounds(new LatLng(37.7642, -122.4604), new LatLng(37.7765, -122.4467));
		
		return m1.requestSummary(bbox)
		.then(stats => {
			expect(stats.amount.length > 0).toBe(true);
			expect(/^[e>][0-9]+$/.test(stats.amount)).toBe(true);
			expect(stats.last > 0).toBe(true);
			expect(stats.bbox).toEqual(bbox.toBBoxString());
		});
	}, REQUEST_TIMEOUT);

	it("requestSummary with valid parameters and options", () => {
		const m1 = new KartaView();
		const bbox = new LatLngBounds(new LatLng(40.724, -73.989), new LatLng(40.725, -73.988));
		
		return m1.requestSummary(bbox, { mindate: 1467377425000 })
		.then(stats => {
			expect(stats.amount.length > 0).toBe(true);
			expect(/^[e>][0-9]+$/.test(stats.amount)).toBe(true);
			expect(stats.last >= 1467377425000).toBe(true);
			expect(stats.bbox).toEqual(bbox.toBBoxString());
		});
	}, REQUEST_TIMEOUT);

	it("requestSummary over empty Paris in january 2017", () => {
		const m1 = new KartaView();
		const bbox = new LatLngBounds(new LatLng(48.84,2.305), new LatLng(48.845,2.31));
		
		return m1.requestSummary(bbox, { mindate: 1483228800000, maxdate: 1484870400000 })
		.then(stats => {
			expect(stats.amount).toEqual("e0");
			expect(stats.last).toEqual(0);
			expect(stats.bbox).toEqual(bbox.toBBoxString());
		});
	}, REQUEST_TIMEOUT);
});

describe("tagsToPictures", () => {
	it("works", () => {
		const m1 = new KartaView();
		const tags = { amenity: "bar", image: "https://storage7.openstreetcam.org/files/photo/2018/3/3/proc/1131761_dda80_44.jpg" };
		const result = m1.tagsToPictures(tags);
		
		expect(result.length).toEqual(1);
		expect(result[0]).toEqual("https://storage7.openstreetcam.org/files/photo/2018/3/3/proc/1131761_dda80_44.jpg");
	});
	
	it("ignores invalid tag value", () => {
		const m1 = new KartaView();
		const tags = { amenity: "bar", image: "NOT A VALID IMAGE" };
		const result = m1.tagsToPictures(tags);
		
		expect(result.length).toEqual(0);
	});
	
	it("handles multiple values", () => {
		const m1 = new KartaView();
		const tags = { amenity: "bar", image: "https://storage7.openstreetcam.org/files/photo/2018/3/3/proc/1131761_dda80_44.jpg;https://storage7.openstreetcam.org/files/photo/2018/3/3/proc/1131761_213f7_45.jpg" };
		const result = m1.tagsToPictures(tags);
		
		expect(result.length).toEqual(2);
		expect(result[0]).toEqual("https://storage7.openstreetcam.org/files/photo/2018/3/3/proc/1131761_dda80_44.jpg");
		expect(result[1]).toEqual("https://storage7.openstreetcam.org/files/photo/2018/3/3/proc/1131761_213f7_45.jpg");
	});
});
