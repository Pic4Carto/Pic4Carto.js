/*
 * Test script for ctrl/fetchers/Flickr.js
 */

import Flickr from "../../../src/lib/ctrl/fetchers/Flickr";
import LatLng from "../../../src/lib/model/LatLng";
import LatLngBounds from "../../../src/lib/model/LatLngBounds";

const API_KEY = "b8c50865a2c22d93d3e673de8bd50178";
const REQUEST_TIMEOUT = 30000;

describe("Constructor", function() {
	it("Constructor with valid parameters", () => {
		const m1 = new Flickr(API_KEY);

		//Check attributes
		expect(m1.apiKey).toEqual(API_KEY);
		expect(m1.name).toEqual("Flickr");
		expect(/(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/.test(m1.homepageUrl)).toBe(true);
		expect(/(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/.test(m1.logoUrl)).toBe(true);
	});

	it("Constructor with missing parameters", () => {
		expect(() => new Flickr()).toThrow("ctrl.fetchers.flickr.invalidapikey");
	});
});

describe("requestPictures", function() {
	it("requestPictures with valid parameters", () => {
		const m1 = new Flickr(API_KEY);
		const bbox = new LatLngBounds(new LatLng(48.850,2.289), new LatLng(48.851,2.290));

		return m1.requestPictures(bbox)
		.then(pictures => {
			expect(pictures.length > 0).toBe(true);

			for(let i=0; i < pictures.length && i < 20; i++) {
				const pic = pictures[i];
				expect(pic.author.length > 0).toBe(true);
				expect(pic.date <= Date.now()).toBe(true);
				expect(pic.date > 0).toBe(true);
				expect(pic.pictureUrl.startsWith("https://live")).toBe(true);
				expect(pic.detailsUrl.startsWith("https://www.flickr.com/photos/")).toBe(true);
				expect(pic.provider).toEqual("Flickr");
				expect(bbox.contains(pic.coordinates)).toBe(true);
				expect(pic.direction).toBe(null);
				expect(pic.osmTags.flickr).toEqual(pic.detailsUrl);
				expect(pic.thumbUrl.startsWith("https://live")).toBe(true);
			}

		});
	}, REQUEST_TIMEOUT);

	it("requestPictures with valid parameters on empty area", () => {
		const m1 = new Flickr(API_KEY);
		const bbox = new LatLngBounds(new LatLng(31.410000000000004,-42.43000000000001), new LatLng(31.420000000000005,-42.42000000000001));

		return m1.requestPictures(bbox)
		.then(pictures => {
			expect(pictures.length).toEqual(0);
		});
	}, REQUEST_TIMEOUT);

	it("requestPictures with valid parameters and options", () => {
		const m1 = new Flickr(API_KEY);
		const bbox = new LatLngBounds(new LatLng(48.85,2.2899999999999996), new LatLng(48.86,2.2999999999999994));

		return m1.requestPictures(bbox, { mindate: 1467377425000, maxdate: 1475326225000 })
		.then(pictures => {
			expect(pictures.length > 0).toBe(true);

			for(let i=0; i < pictures.length && i < 20; i++) {
				const pic = pictures[i];
				expect(pic.author.length > 0).toBe(true);
				expect(pic.date <= 1475326225000).toBe(true);
				expect(pic.date >= 1467377425000).toBe(true);
				expect(pic.pictureUrl.startsWith("https://live")).toBe(true);
				expect(pic.detailsUrl.startsWith("https://www.flickr.com/photos/")).toBe(true);
				expect(pic.provider).toEqual("Flickr");
				expect(bbox.contains(pic.coordinates)).toBe(true);
				expect(pic.direction).toBe(null);
			}
		});
	}, REQUEST_TIMEOUT);
});

describe("requestSummary", function() {
	it("requestSummary with valid parameters", () => {
		const m1 = new Flickr(API_KEY);
		const bbox = new LatLngBounds(new LatLng(48.85,2.2899999999999996), new LatLng(48.86,2.2999999999999994));

		return m1.requestSummary(bbox)
		.then(stats => {
			expect(stats.amount.length > 0).toBe(true);
			expect(/^[e>][0-9]+$/.test(stats.amount)).toBe(true);
			expect(stats.last > 0).toBe(true);
			expect(stats.bbox).toEqual(bbox.toBBoxString());

		});
	}, REQUEST_TIMEOUT);

	it("requestSummary with valid parameters and options", () => {
		const m1 = new Flickr(API_KEY);
		const bbox = new LatLngBounds(new LatLng(48.85,2.2899999999999996), new LatLng(48.86,2.2999999999999994));

		return m1.requestSummary(bbox, { mindate: 1467377425000, maxdate: 1475326225000 })
		.then(stats => {
			expect(stats.amount.length > 0).toBe(true);
			expect(/^[e>][0-9]+$/.test(stats.amount)).toBe(true);
			expect(stats.last >= 1467377425000).toBe(true);
			expect(stats.last <= 1475326225000).toBe(true);
			expect(stats.bbox).toEqual(bbox.toBBoxString());

		});
	}, REQUEST_TIMEOUT);
});
