/*
 * Test script for ctrl/fetchers/Panoramax.js
 */


import LatLng from "../../../src/lib/model/LatLng";
import LatLngBounds from "../../../src/lib/model/LatLngBounds";
import Panoramax from "../../../src/lib/ctrl/fetchers/Panoramax";

const REQUEST_TIMEOUT = 30000;

describe("Constructor", function() {
	it("Constructor with valid parameters", () => {
		const m1 = new Panoramax();
		
		//Check attributes
		expect(m1.name).toEqual("Panoramax");
		expect(/https:\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/.test(m1.homepageUrl)).toBe(true);
		expect(/https:\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/.test(m1.logoUrl)).toBe(true);
	});
});

describe("requestPictures", function() {
	it("requestPictures with valid parameters", () => {
		const m1 = new Panoramax();
		const bbox = new LatLngBounds(new LatLng(48.132752, -1.7107734), new LatLng(48.1329716, -1.7101578));
		
		return m1.requestPictures(bbox)
		.then(pictures => {
			expect(pictures.length > 0).toBe(true);
			
			const pic1 = pictures[0];
			expect(pic1.author.length > 0).toBe(true);
			expect(pic1.license).toEqual("CC-BY-SA-4.0");
			expect(pic1.date > 0).toBe(true);
			expect(pic1.pictureUrl.startsWith("https://panoramax")).toBe(true);
			expect(pic1.detailsUrl.startsWith("https://api.panoramax")).toBe(true);
			expect(pic1.provider).toEqual("Panoramax");
			expect(bbox.contains(pic1.coordinates)).toBe(true);
			expect(pic1.direction === null || !isNaN(pic1.direction)).toBe(true);
			expect(pic1.osmTags.panoramax.length > 0).toBe(true);
		});
	}, REQUEST_TIMEOUT);

	it("requestPictures with valid parameters on empty area", () => {
		const m1 = new Panoramax();
		const bbox = new LatLngBounds(new LatLng(42.22251,-17.85302), new LatLng(42.22252,-17.85301));

		return m1.requestPictures(bbox)
		.then(pictures => {
			expect(pictures.length).toEqual(0);
		});
	}, REQUEST_TIMEOUT);

	it("requestPictures with valid parameters and options defined", () => {
		const m1 = new Panoramax();
		const bbox = new LatLngBounds(new LatLng(48.132752, -1.7107734), new LatLng(48.1329716, -1.7101578));
		
		return m1.requestPictures(bbox, { mindate: 1453972647000 })
		.then(pictures => {
			expect(pictures.length > 0).toBe(true);
			
			const pic1 = pictures[0];
			expect(pic1.author.length > 0).toBe(true);
			expect(pic1.license).toEqual("CC-BY-SA-4.0");
			expect(pic1.date >= 1453972647000).toBe(true);
			expect(pic1.pictureUrl.startsWith("https://panoramax")).toBe(true);
			expect(pic1.detailsUrl.startsWith("https://api.panoramax")).toBe(true);
			expect(pic1.provider).toEqual("Panoramax");
			expect(bbox.contains(pic1.coordinates)).toBe(true);
			expect(pic1.direction === null || !isNaN(pic1.direction)).toBe(true);
			expect(pic1.osmTags.panoramax.length > 0).toBe(true);
		});
	}, REQUEST_TIMEOUT);
});

describe("requestSummary", function() {
	it("requestSummary with valid parameters", () => {
		const m1 = new Panoramax();
		const bbox = new LatLngBounds(new LatLng(48.132752, -1.7107734), new LatLng(48.1329716, -1.7101578));
		
		return m1.requestSummary(bbox)
		.then(stats => {
			expect(stats.amount.length > 0).toBe(true);
			expect(/^[e>][0-9]+$/.test(stats.amount)).toBe(true);
			expect(stats.last > 0).toBe(true);
			expect(stats.bbox).toEqual(bbox.toBBoxString());
		});
	}, REQUEST_TIMEOUT);

	it("requestSummary with valid parameters and options", () => {
		const m1 = new Panoramax();
		const bbox = new LatLngBounds(new LatLng(48.132752, -1.7107734), new LatLng(48.1329716, -1.7101578));
		
		return m1.requestSummary(bbox, { mindate: 1453972647000 })
		.then(stats => {
			expect(stats.amount.length > 0).toBe(true);
			expect(/^[e>][0-9]+$/.test(stats.amount)).toBe(true);
			expect(stats.last >= 1453972647000).toBe(true);
			expect(stats.bbox).toEqual(bbox.toBBoxString());
		});
	}, REQUEST_TIMEOUT);
});

describe("tagsToPictures", () => {
	it("works", () => {
		const m1 = new Panoramax();
		const tags = { amenity: "bar", panoramax: "blabla-bla-bla-bla", "panoramax:front": "bloublou-blou", "panoramax:whatever": "https://pano.ra/max.jpg" };
		const result = m1.tagsToPictures(tags);
		
		expect(result.length).toEqual(3);
		expect(result[0]).toEqual("https://api.panoramax.xyz/api/pictures/blabla-bla-bla-bla/hd.jpg");
		expect(result[1]).toEqual("https://api.panoramax.xyz/api/pictures/bloublou-blou/hd.jpg");
		expect(result[2]).toEqual("https://pano.ra/max.jpg");
	});
	
	it("handles multiple values", () => {
		const m1 = new Panoramax();
		const tags = { amenity: "bar", panoramax: "blabla;bloublou" };
		const result = m1.tagsToPictures(tags);
		
		expect(result.length).toEqual(2);
		expect(result[0]).toEqual("https://api.panoramax.xyz/api/pictures/blabla/hd.jpg");
		expect(result[1]).toEqual("https://api.panoramax.xyz/api/pictures/bloublou/hd.jpg");
	});
});
