/*
 * Test script for ctrl/fetchers/Mapillary.js
 */


import LatLng from "../../../src/lib/model/LatLng";
import LatLngBounds from "../../../src/lib/model/LatLngBounds";
import Detection from "../../../src/lib/model/Detection";
import Mapillary from "../../../src/lib/ctrl/fetchers/Mapillary";

const CLIENT_ID = "MLY|4186514921455326|31c8e0e5704a790a12cdb71c37dd4964";
const REQUEST_TIMEOUT = 30000;

describe("Constructor", function() {
	it("Constructor with valid parameters", () => {
		const m1 = new Mapillary(CLIENT_ID);

		//Check attributes
		expect(m1.clientId).toEqual(CLIENT_ID);
		expect(m1.name).toEqual("Mapillary");
		expect(/(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/.test(m1.homepageUrl)).toBe(true);
		expect(/(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/.test(m1.logoUrl)).toBe(true);
	});

	it("Constructor with missing parameters", () => {
		expect(() => new Mapillary()).toThrow("ctrl.fetchers.mapillary.invalidclientid");
	});
});

describe("requestPictures", function() {
	it("requestPictures with valid parameters", () => {
		const m1 = new Mapillary(CLIENT_ID);
		const bbox = new LatLngBounds(new LatLng(48.155591,-1.702972), new LatLng(48.155720,-1.702795));

		return m1.requestPictures(bbox)
		.then(pictures => {
			expect(pictures.length > 0).toBe(true);

			const pic1 = pictures[1];
			expect(pic1.author.length > 0).toBe(true);
			expect(pic1.license).toEqual("CC By-SA 4.0");
			expect(pic1.date > 0).toBe(true);
			expect(pic1.pictureUrl.startsWith("https://")).toBe(true);
			expect(pic1.detailsUrl.startsWith("https://www.mapillary.com/app/?pKey=")).toBe(true);
			expect(pic1.provider).toEqual("Mapillary");
			expect(bbox.contains(pic1.coordinates)).toBe(true);
			expect(typeof pic1.direction).toEqual("number");
			expect(pic1.osmTags.mapillary.match(/^[a-zA-Z0-9_\-]+$/)).toBeTruthy();
			expect(pic1.thumbUrl.startsWith("https://")).toBe(true);

		});
	}, REQUEST_TIMEOUT);

	it("requestPictures with valid parameters on empty area", () => {
		const m1 = new Mapillary(CLIENT_ID);
		const bbox = new LatLngBounds(new LatLng(46.04801, -23.22835), new LatLng(46.04802, -23.22834));

		return m1.requestPictures(bbox)
		.then(pictures => {
			expect(pictures.length).toEqual(0);
		});
	}, REQUEST_TIMEOUT);

	it("requestPictures with valid parameters and options defined", () => {
		const m1 = new Mapillary(CLIENT_ID);
		const bbox = new LatLngBounds(new LatLng(48.155591,-1.702972), new LatLng(48.155720,-1.702795));

		return m1.requestPictures(bbox, { mindate: 1467377425000, maxdate: 1475326225000 })
		.then(pictures => {
			expect(pictures.length > 0).toBe(true);

			const pic1 = pictures[1];
			expect(pic1.author.length > 0).toBe(true);
			expect(pic1.license).toEqual("CC By-SA 4.0");
			expect(pic1.date >= 1467377425000).toBe(true);
			expect(pic1.date <= 1475326225000).toBe(true);
			expect(pic1.pictureUrl.startsWith("https://")).toBe(true);
			expect(pic1.detailsUrl.startsWith("https://www.mapillary.com/app/?pKey=")).toBe(true);
			expect(pic1.provider).toEqual("Mapillary");
			expect(bbox.contains(pic1.coordinates)).toBe(true);
			expect(typeof pic1.direction).toEqual("number");
		});
	}, REQUEST_TIMEOUT);
});

describe("requestSummary", function() {
	it("requestSummary with valid parameters", () => {
		const m1 = new Mapillary(CLIENT_ID);
		const bbox = new LatLngBounds(new LatLng(48.155591,-1.702972), new LatLng(48.155720,-1.702795));

		return m1.requestSummary(bbox)
		.then(stats => {
			expect(stats.amount.length > 0).toBe(true);
			expect(/^[e>][0-9]+$/.test(stats.amount)).toBe(true);
			expect(stats.last > 0).toBe(true);
			expect(stats.bbox).toEqual(bbox.toBBoxString());
		});
	}, REQUEST_TIMEOUT);

	it("requestSummary with valid parameters and options", () => {
		const m1 = new Mapillary(CLIENT_ID);
		const bbox = new LatLngBounds(new LatLng(48.155591,-1.702972), new LatLng(48.155720,-1.702795));

		return m1.requestSummary(bbox, { mindate: 1467377425000, maxdate: 1475326225000 })
		.then(stats => {
			expect(stats.amount.length > 0).toBe(true);
			expect(/^[e>][0-9]+$/.test(stats.amount)).toBe(true);
			expect(stats.last >= 1467377425000).toBe(true);
			expect(stats.last <= 1475326225000).toBe(true);
			expect(stats.bbox).toEqual(bbox.toBBoxString());
		});
	}, REQUEST_TIMEOUT);
});

describe("requestDetections", () => {
	it("works with valid parameters", () => {
		const m1 = new Mapillary(CLIENT_ID);
		const bbox = new LatLngBounds(new LatLng(48.12902, -1.67708), new LatLng(48.13101, -1.67515));

		return m1.requestDetections(bbox, { types: [ Detection.SIGN_STOP ]})
		.then(features => {
			expect(features.length > 0).toBe(true);
			features.forEach(f => {
				expect(f instanceof Detection).toBe(true);
				expect(f.provider).toEqual("mapillary");
				expect(f.type).toEqual(Detection.SIGN_STOP);
			});
		});
	}, REQUEST_TIMEOUT);
});

describe("tagsToPictures", () => {
	it("works", () => {
		const m1 = new Mapillary(CLIENT_ID);
		const tags = { amenity: "bar", mapillary: "87Fu43CBg7Ls17kUnpRv4w" };
		const result = m1.tagsToPictures(tags);

		expect(result.length).toEqual(1);
		expect(result[0]).toEqual("https://images.mapillary.com/87Fu43CBg7Ls17kUnpRv4w/thumb-2048.jpg");
	});

	it("ignores invalid tag value", () => {
		const m1 = new Mapillary(CLIENT_ID);
		const tags = { amenity: "bar", mapillary: "Not a mapillary key" };
		const result = m1.tagsToPictures(tags);

		expect(result.length).toEqual(0);
	});

	it("handles multiple values", () => {
		const m1 = new Mapillary(CLIENT_ID);
		const tags = { amenity: "bar", mapillary: "87Fu43CBg7Ls17kUnpRv4w;x8cFRs-Zmpmjeb_9SgeuvA", "mapillary:N": "B4N8Yx4R_n4Lp02kRU8T3Q" };
		const result = m1.tagsToPictures(tags);

		expect(result.length).toEqual(3);
		expect(result[0]).toEqual("https://images.mapillary.com/87Fu43CBg7Ls17kUnpRv4w/thumb-2048.jpg");
		expect(result[1]).toEqual("https://images.mapillary.com/x8cFRs-Zmpmjeb_9SgeuvA/thumb-2048.jpg");
		expect(result[2]).toEqual("https://images.mapillary.com/B4N8Yx4R_n4Lp02kRU8T3Q/thumb-2048.jpg");
	});
});
