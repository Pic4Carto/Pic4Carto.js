/*
 * Test script for ctrl/fetchers/WikiCommons.js
 */


import LatLng from "../../../src/lib/model/LatLng";
import LatLngBounds from "../../../src/lib/model/LatLngBounds";
import WikiCommons from "../../../src/lib/ctrl/fetchers/WikiCommons";

const REQUEST_TIMEOUT = 30000;

describe("Constructor", function() {
	it("Constructor with valid parameters", () => {
		const m1 = new WikiCommons();
		
		//Check attributes
		expect(m1.name).toEqual("Wikimedia Commons");
		expect(/(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/.test(m1.homepageUrl)).toBe(true);
		expect(/(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/.test(m1.logoUrl)).toBe(true);
	});
});

describe("requestPictures", function() {
	it("requestPictures with valid parameters", () => {
		const m1 = new WikiCommons();
		const bbox = new LatLngBounds(new LatLng(48.85,2.2899999999999996), new LatLng(48.86,2.2999999999999994));
		
		return m1.requestPictures(bbox)
		.then(pictures => {
			expect(pictures.length > 0).toBe(true);
			
			for(let i=0; i < pictures.length && i < 20; i++) {
				const pic = pictures[i];
				expect(pic.author.length > 0).toBe(true);
				expect(pic.date <= Date.now()).toBe(true);
				expect(pic.date > 0).toBe(true);
				expect(pic.pictureUrl.startsWith("https://upload.wikimedia.org/wikipedia/commons/")).toBe(true);
				expect(pic.detailsUrl.startsWith("https://commons.wikimedia.org/wiki/File:")).toBe(true);
				expect(pic.provider).toEqual("Wikimedia Commons");
				expect(bbox.contains(pic.coordinates)).toBe(true);
				expect(pic.direction).toEqual(null);
				expect(pic.osmTags.wikimedia_commons.startsWith("File:")).toBe(true);
				expect(pic.thumbUrl.startsWith("https://upload.wikimedia.org/wikipedia/commons/")).toBe(true);
			}
		});
	}, REQUEST_TIMEOUT);

	it("requestPictures with valid parameters on empty area", () => {
		const m1 = new WikiCommons();
		const bbox = new LatLngBounds(new LatLng(31.410000000000004,-42.43000000000001), new LatLng(31.420000000000005,-42.42000000000001));
		
		return m1.requestPictures(bbox)
		.then(pictures => {
			expect(pictures.length).toEqual(0);
		});
	}, REQUEST_TIMEOUT);

	it("requestPictures with valid parameters and options", () => {
		const m1 = new WikiCommons();
		const bbox = new LatLngBounds(new LatLng(48.85,2.2899999999999996), new LatLng(48.86,2.2999999999999994));
		
		return m1.requestPictures(bbox, { mindate: 1467377425000, maxdate: 1475326225000 })
		.then(pictures => {
			expect(pictures.length > 0).toBe(true);
			
			for(let i=0; i < pictures.length && i < 20; i++) {
				const pic = pictures[i];
				expect(pic.author.length > 0).toBe(true);
				expect(pic.date <= 1475326225000).toBe(true);
				expect(pic.date >= 1467377425000).toBe(true);
				expect(pic.pictureUrl.startsWith("https://upload.wikimedia.org/wikipedia/commons/")).toBe(true);
				expect(pic.detailsUrl.startsWith("https://commons.wikimedia.org/wiki/File:")).toBe(true);
				expect(pic.provider).toEqual("Wikimedia Commons");
				expect(bbox.contains(pic.coordinates)).toBe(true);
				expect(pic.direction).toBe(null);
			}
		});
	}, REQUEST_TIMEOUT);
});

describe("requestSummary", function() {
	it("requestSummary with valid parameters", () => {
		const m1 = new WikiCommons();
		const bbox = new LatLngBounds(new LatLng(48.85,2.2899999999999996), new LatLng(48.86,2.2999999999999994));
		
		return m1.requestSummary(bbox)
		.then(stats => {
			expect(stats.amount.length > 0).toBe(true);
			expect(/^[e>][0-9]+$/.test(stats.amount)).toBe(true);
			expect(stats.last > 0).toBe(true);
			expect(stats.bbox).toEqual(bbox.toBBoxString());
		});
	}, REQUEST_TIMEOUT);

	it("requestSummary with valid parameters and options", () => {
		const m1 = new WikiCommons();
		const bbox = new LatLngBounds(new LatLng(48.85,2.2899999999999996), new LatLng(48.86,2.2999999999999994));
		
		return m1.requestSummary(bbox, { mindate: 1467377425000, maxdate: 1475326225000 })
		.then(stats => {
			expect(stats.amount.length > 0).toBe(true);
			expect(/^[e>][0-9]+$/.test(stats.amount)).toBe(true);
			expect(stats.last >= 1467377425000).toBe(true);
			expect(stats.last <= 1475326225000).toBe(true);
			expect(stats.bbox).toEqual(bbox.toBBoxString());
		});
	}, REQUEST_TIMEOUT);
});

describe("tagsToPictures", () => {
	it("works", () => {
		const m1 = new WikiCommons();
		const tags = { amenity: "bar", wikimedia_commons: "File:Nasir-al molk -1.jpg" };
		const result = m1.tagsToPictures(tags);
		
		expect(result.length).toEqual(1);
		expect(result[0]).toEqual("https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Nasir-al_molk_-1.jpg/1024px-Nasir-al_molk_-1.jpg");
	});
	
	it("ignores invalid tag value", () => {
		const m1 = new WikiCommons();
		const tags = { amenity: "bar", wikimedia_commons: "Not a wikimedia commons file" };
		const result = m1.tagsToPictures(tags);
		
		expect(result.length).toEqual(0);
	});
	
	it("handles multiple values", () => {
		const m1 = new WikiCommons();
		const tags = { amenity: "bar", wikimedia_commons: "File:Nasir-al molk -1.jpg;File:BlueMarble-2001-2002.jpg" };
		const result = m1.tagsToPictures(tags);
		
		expect(result.length).toEqual(2);
		expect(result[0]).toEqual("https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Nasir-al_molk_-1.jpg/1024px-Nasir-al_molk_-1.jpg");
		expect(result[1]).toEqual("https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/BlueMarble-2001-2002.jpg/1024px-BlueMarble-2001-2002.jpg");
	});
});
