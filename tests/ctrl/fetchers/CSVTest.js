/*
 * Test script for ctrl/fetchers/CSV.js
 */


import LatLng from "../../../src/lib/model/LatLng";
import LatLngBounds from "../../../src/lib/model/LatLngBounds";
import CSV from "../../../src/lib/ctrl/fetchers/CSV";
import fs from 'fs';
import path from 'path';

jest.mock("kdbush", () => (
	class KDBush {
		constructor() {
			this.data = [];
		}
		add(x, y) {
			this.data.push([x, y]);
		}
		finish() {
			console.log(this.data);
		}
		range(w,s,e,n) {
			let ids = [];
			for(let i=0; i < this.data.length; i++) {
				let d = this.data[i];
				if(d[0]>=w && d[0]<=e && d[1]>=s && d[1]<=n) {
					ids.push(i);
				}
			}
			return ids;
		}
	}
));

const csv1 = fs.readFileSync(path.resolve(__dirname, "../../assets/csv_fetcher_1.csv"), "utf8");
const csv2 = fs.readFileSync(path.resolve(__dirname, "../../assets/csv_fetcher_2.csv"), "utf8");
const csvFetcher = jest.fn(url =>
	Promise.resolve({
		text: () => {
			if(url.includes("csv_fetcher_1")) { return csv1; }
			else { return csv2; }
		},
	})
);

const REQUEST_TIMEOUT = 30000;

describe("Constructor", function() {
	it("Handles valid parameters", () => {
		const m1 = new CSV("http://myserver.net/data.csv");
		
		//Check attributes
		expect(m1.csvURL).toEqual("http://myserver.net/data.csv");
		expect(m1.name).toEqual("CSV Source");
		expect(m1.homepageUrl).toEqual("");
		expect(m1.logoUrl).toEqual("");
	});

	it("Handles missing parameters", () => {
		expect(() => new CSV()).toThrow("ctrl.fetchers.csv.invalidcsvurl");
	});
	
	it("Handles optional parameters", () => {
		const m1 = new CSV("http://myserver.net/data.csv", {
			name: "Test of CSV",
			homepage: "http://test.net/csv",
			logo: "http://test.net/logo",
			license: "CC0",
			user: "Anon",
			bbox: new LatLngBounds(new LatLng(1,2), new LatLng(3,4))
		});
		
		//Check attributes
		expect(m1.csvURL).toEqual("http://myserver.net/data.csv");
		expect(m1.name).toEqual("Test of CSV");
		expect(m1.homepageUrl).toEqual("http://test.net/csv");
		expect(m1.logoUrl).toEqual("http://test.net/logo");
		expect(m1.options.license).toEqual("CC0");
		expect(m1.options.user).toEqual("Anon");
		expect(m1.options.bbox.equals(new LatLngBounds(new LatLng(1,2), new LatLng(3,4)))).toBe(true);
	});
});

describe("requestPictures", function() {
	global.fetch = csvFetcher;

	it("with valid parameters", () => {
		const m1 = new CSV("http://myserver.net/csv_fetcher_1.csv");
		const bbox = new LatLngBounds(new LatLng(-0.005, -0.005), new LatLng(0.0015, 0.0015));
		
		return m1.requestPictures(bbox)
		.then(pictures => {
			
			expect(pictures.length).toEqual(2);
			
			const pic1 = pictures[0];
			expect(pic1.author).toEqual("User #1");
			expect(pic1.license).toEqual("CC By-Sa 2.0");
			expect(pic1.date).toEqual(1234000);
			expect(pic1.pictureUrl).toEqual("http://myserver.net/images/001.jpg");
			expect(pic1.detailsUrl).toEqual("http://myserver.net/details/001");
			expect(pic1.provider).toEqual("CSV Source");
			expect(bbox.contains(pic1.coordinates)).toBe(true);
			expect(pic1.direction).toEqual(152);
			expect(pic1.osmTags.image).toEqual("http://myserver.net/images/001.jpg");
			
			const pic2 = pictures[1];
			expect(pic2.author).toEqual("User #3");
			expect(pic2.license).toEqual("CC0");
			expect(pic2.date).toEqual(1236000);
			expect(pic2.pictureUrl).toEqual("http://myserver.net/images/003.jpg");
			expect(pic2.detailsUrl).toEqual("http://myserver.net/details/003");
			expect(pic2.provider).toEqual("CSV Source");
			expect(bbox.contains(pic2.coordinates)).toBe(true);
			expect(pic2.direction).toEqual(42);
			expect(pic2.osmTags.image).toEqual("http://myserver.net/images/003.jpg");
		});
	}, REQUEST_TIMEOUT);

	it("with valid parameters on empty area", () => {
		const m1 = new CSV("http://myserver.net/csv_fetcher_1.csv");
		const bbox = new LatLngBounds(new LatLng(-1, -1), new LatLng(-0.5, -0.5));

		return m1.requestPictures(bbox)
		.then(pictures => {
			expect(pictures.length).toEqual(0);
		});
	}, REQUEST_TIMEOUT);

	it("with valid parameters and date filtering", () => {
		
		const m1 = new CSV("http://myserver.net/csv_fetcher_1.csv");
		const bbox = new LatLngBounds(new LatLng(-1, -1), new LatLng(1, 1));
		
		return m1.requestPictures(bbox, { mindate: 0, maxdate: 1234000 })
		.then(pictures => {
			expect(pictures.length > 0).toBe(true);
			
			for(const p of pictures) {
				expect(p.date >= 0 && p.date <= 1234000).toBe(true);
			}
			
		});
	}, REQUEST_TIMEOUT);
	
	it("with valid parameters and use of default fetcher params", () => {
		
		const m1 = new CSV("http://myserver.net/csv_fetcher_2.csv", {
			license: "default license",
			user: "default user"
		});
		const bbox = new LatLngBounds(new LatLng(-1, -1), new LatLng(1, 1));
		
		return m1.requestPictures(bbox)
		.then(pictures => {
			expect(pictures.length > 0).toBe(true);
			
			expect(pictures[0].author).toEqual("default user");
			expect(pictures[1].license).toEqual("default license");
			expect(pictures[2].detailsUrl).toEqual(pictures[2].pictureUrl);
			expect(pictures[3].direction).toEqual(null);
			
		});
	}, REQUEST_TIMEOUT);
	
	it("with valid parameters with data bbox defined", () => {
		
		const m1 = new CSV("http://myserver.net/csv_fetcher_1.csv", {
			bbox: new LatLngBounds(new LatLng(3,3), new LatLng(4,4))
		});
		const bbox = new LatLngBounds(new LatLng(-1, -1), new LatLng(-0.5, -0.5));

		return m1.requestPictures(bbox)
		.then(pictures => {
			expect(pictures.length).toEqual(0);
		});
	}, REQUEST_TIMEOUT);
});

describe("requestSummary", function() {
	global.fetch = csvFetcher;

	it("requestSummary with valid parameters", () => {
		
		const m1 = new CSV("http://myserver.net/csv_fetcher_1.csv");
		const bbox = new LatLngBounds(new LatLng(-0.005, -0.005), new LatLng(0.0015, 0.0015));
		
		return m1.requestSummary(bbox)
		.then(stats => {
			expect(stats.amount.length > 0).toBe(true);
			expect(/^[e>][0-9]+$/.test(stats.amount)).toBe(true);
			expect(stats.last > 0).toBe(true);
			expect(stats.bbox).toEqual(bbox.toBBoxString());
			
		});
	}, REQUEST_TIMEOUT);

	it("requestSummary with valid parameters and options", () => {
		
		const m1 = new CSV("http://myserver.net/csv_fetcher_1.csv");
		const bbox = new LatLngBounds(new LatLng(-0.005, -0.005), new LatLng(0.0015, 0.0015));
		
		return m1.requestSummary(bbox, { mindate: 0, maxdate: 1234000 })
		.then(stats => {
			expect(stats.amount.length > 0).toBe(true);
			expect(/^[e>][0-9]+$/.test(stats.amount)).toBe(true);
			expect(stats.last >= 0).toBe(true);
			expect(stats.last <= 1234000).toBe(true);
			expect(stats.bbox).toEqual(bbox.toBBoxString());
		});
	}, REQUEST_TIMEOUT);
});
