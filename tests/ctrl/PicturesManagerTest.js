/**
 * Test script for PicturesManager
 */


import LatLng from "../../src/lib/model/LatLng";
import LatLngBounds from "../../src/lib/model/LatLngBounds";
import Picture from "../../src/lib/model/Picture";
import Detection from "../../src/lib/model/Detection";
import PicturesManager from "../../src/lib/ctrl/PicturesManager";
import fs from 'fs';
import path from 'path';

jest.mock("kdbush", () => (
	class KDBush {
		constructor() {
			this.data = [];
		}
		add(x, y) {
			this.data.push([x, y]);
		}
		finish() {
			console.log(this.data);
		}
		range(w,s,e,n) {
			let ids = [];
			for(let i=0; i < this.data.length; i++) {
				let d = this.data[i];
				if(d[0]>=w && d[0]<=e && d[1]>=s && d[1]<=n) {
					ids.push(i);
				}
			}
			return ids;
		}
	}
));

const REQUEST_TIMEOUT = 30000;

const csv1 = fs.readFileSync(path.resolve(__dirname, "../assets/csv_fetcher_1.csv"), "utf8");
const csvFetcher = jest.fn((url, opts) => url.includes("csv_fetcher_1") ?
	Promise.resolve({text: () => csv1})
	: fetch(url, opts)
);

describe("Constructor", function() {
	it("handles user-defined fetchers", function() {
		const pm1 = new PicturesManager({
			userDefinedFetchers: {
				f1: {
					csv: "http://server1.net/index.csv",
					name: "F1",
					logo: "http://server1.net/logo.png",
					homepage: "http://server1.net/",
					bbox: new LatLngBounds(new LatLng(0,0), new LatLng(10,10)),
					license: "Custom license",
					user: "Custom user"
				},
				f2: {
					csv: "http://server2.net/index.csv"
				}
			}
		});

		expect(pm1.fetchers.f1.csvURL).toEqual("http://server1.net/index.csv");
		expect(pm1.fetchers.f1.name).toEqual("F1");
		expect(pm1.fetchers.f1.logoUrl).toEqual("http://server1.net/logo.png");
		expect(pm1.fetchers.f1.homepageUrl).toEqual("http://server1.net/");
		expect(pm1.fetchers.f1.options.bbox.equals(new LatLngBounds(new LatLng(0,0), new LatLng(10,10)))).toBe(true);
		expect(pm1.fetchers.f1.options.license).toEqual("Custom license");
		expect(pm1.fetchers.f1.options.user).toEqual("Custom user");

		expect(pm1.fetchers.f2.csvURL).toEqual("http://server2.net/index.csv");
	});

	it("fails if udf misses csv parameter", function() {
		expect(() => new PicturesManager({
				userDefinedFetchers: {
					f1: {
						name: "F1",
						logo: "http://server1.net/logo.png",
						homepage: "http://server1.net/",
						bbox: new LatLngBounds(new LatLng(0,0), new LatLng(10,10)),
						license: "Custom license",
						user: "Custom user"
					}
				}
			})
		).toThrow("Can't create user-defined fetcher f1: invalid csv parameter (must be an URL)");
	});
});

describe("getFetcherDetails", function() {
	it("returns fetchers details", () => {
		const dm1 = new PicturesManager({
			userDefinedFetchers: {
				f1: {
					name: "F1",
					csv: "http://server1.net/index.csv",
					logo: "http://server1.net/logo.png",
					homepage: "http://server1.net/",
					bbox: new LatLngBounds(new LatLng(0,0), new LatLng(10,10)),
					license: "Custom license",
					user: "Custom user"
				}
			}
		});
		const result = dm1.getFetcherDetails();
		const rgxUrl = /(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/;

		expect(Object.keys(result).length).toEqual(5);

		expect(result.mapillary.name).toEqual("Mapillary");
		expect(rgxUrl.test(result.mapillary.homepageUrl)).toBe(true);
		expect(rgxUrl.test(result.mapillary.logoUrl)).toBe(true);

		expect(result.flickr.name).toEqual("Flickr");
		expect(rgxUrl.test(result.flickr.homepageUrl)).toBe(true);
		expect(rgxUrl.test(result.flickr.logoUrl)).toBe(true);

		expect(result.kartaview.name).toEqual("KartaView");
		expect(rgxUrl.test(result.kartaview.homepageUrl)).toBe(true);
		expect(rgxUrl.test(result.kartaview.logoUrl)).toBe(true);

		expect(result.wikicommons.name).toEqual("Wikimedia Commons");
		expect(rgxUrl.test(result.wikicommons.homepageUrl)).toBe(true);
		expect(rgxUrl.test(result.wikicommons.logoUrl)).toBe(true);

		expect(result.f1.name).toEqual("F1");
		expect(rgxUrl.test(result.f1.homepageUrl)).toBe(true);
		expect(rgxUrl.test(result.f1.logoUrl)).toBe(true);
	});
});

describe("startPicsRetrieval", function() {
	it("over not empty area", () => {
		const bbox = new LatLngBounds(new LatLng(48.10,-1.69), new LatLng(48.105,-1.685));
		const dm1 = new PicturesManager();

		return dm1.startPicsRetrieval(bbox)
		.then(picsArray => {
			expect(picsArray.length > 0).toBe(true);

			for(let i=0; i < picsArray.length; i++) {
				const pic = picsArray[i];
				expect(pic instanceof Picture).toBe(true);
				expect(bbox.contains(pic.coordinates)).toBe(true);
				expect(pic.date > 0).toBe(true);
			}
		});
	}, REQUEST_TIMEOUT);

	it("over empty area", () => {
		const bbox = new LatLngBounds(new LatLng(35.75, -44.78), new LatLng(35.76, -44.77));
		const dm1 = new PicturesManager();

		return dm1.startPicsRetrieval(bbox)
		.then(picsArray => {
			expect(picsArray.length).toEqual(0);
		});
	}, REQUEST_TIMEOUT);

	it("over not empty area and options given", () => {
		const bbox = new LatLngBounds(new LatLng(48.10,-1.69), new LatLng(48.105,-1.685));
		const dm1 = new PicturesManager();

		return dm1.startPicsRetrieval(bbox, { mindate: 1467377425000, maxdate: 1475326225000, cameraAngle: 120 })
		.then(picsArray => {
			expect(picsArray.length > 0).toBe(true);

			for(let i=0; i < picsArray.length; i++) {
				const pic = picsArray[i];
				expect(pic instanceof Picture).toBe(true);
				expect(bbox.contains(pic.coordinates)).toBe(true);
				expect(pic.date >= 1467377425000).toBe(true);
				expect(pic.date <= 1475326225000).toBe(true);
			}
		});
	}, REQUEST_TIMEOUT);

	it("over not empty area and usefetchers option defined", () => {
		const bbox = new LatLngBounds(new LatLng(48.10,-1.69), new LatLng(48.105,-1.685));
		const dm1 = new PicturesManager();
		const fetchers = [ "flickr", "wikicommons" ];

		return dm1.startPicsRetrieval(bbox, { usefetchers: fetchers })
		.then(picsArray => {
			expect(picsArray.length > 0).toBe(true);

			for(let i=0; i < picsArray.length; i++) {
				const pic = picsArray[i];
				expect(pic instanceof Picture).toBe(true);
				expect(bbox.contains(pic.coordinates)).toBe(true);
				expect(pic.provider == "Flickr" || pic.provider == "Wikimedia Commons").toBe(true);
			}
		});
	}, REQUEST_TIMEOUT);

	it("over not empty area and usefetchers too restrictive", () => {
		const bbox = new LatLngBounds(new LatLng(48.10,-1.69), new LatLng(48.105,-1.685));
		const dm1 = new PicturesManager();
		const fetchers = [ "nope" ];

		expect(() => dm1.startPicsRetrieval(bbox, { usefetchers: fetchers }))
		.toThrow("ctrl.picturesmanager.picsretrieval.nofetchersused");
	});

	it("over not empty area and ignorefetchers option defined", () => {
		const bbox = new LatLngBounds(new LatLng(48.10,-1.69), new LatLng(48.105,-1.685));
		const dm1 = new PicturesManager();
		const ignoredFetchers = [ "mapillary" ];

		return dm1.startPicsRetrieval(bbox, { ignorefetchers: ignoredFetchers })
		.then(picsArray => {
			expect(picsArray.length > 0).toBe(true);

			for(let i=0; i < picsArray.length; i++) {
				const pic = picsArray[i];
				expect(pic instanceof Picture).toBe(true);
				expect(bbox.contains(pic.coordinates)).toBe(true);
				expect(pic.provider != "Mapillary").toBe(true);
			}
		});
	}, REQUEST_TIMEOUT);

	it("over not empty area and ignorefetchers too restrictive", () => {
		const bbox = new LatLngBounds(new LatLng(48.10,-1.69), new LatLng(48.105,-1.685));
		const dm1 = new PicturesManager();
		const ignoredFetchers = [ "mapillary", "flickr", "wikicommons", "kartaview" ];

		expect(() => dm1.startPicsRetrieval(bbox, { ignorefetchers: ignoredFetchers }))
		.toThrow("ctrl.picturesmanager.picsretrieval.nofetchersused");
	});

	it("over not empty area and both ignorefetchers and usefetchers options defined", () => {
		const bbox = new LatLngBounds(new LatLng(48.10,-1.69), new LatLng(48.105,-1.685));
		const dm1 = new PicturesManager();
		const usef = [ "mapillary", "wikicommons" ];
		const ignf = [ "mapillary", "flickr" ];

		return dm1.startPicsRetrieval(bbox, { usefetchers: usef, ignorefetchers: ignf })
		.then(picsArray => {
			expect(picsArray.length > 0).toBe(true);
			let mapillaryPics = 0;

			for(let i=0; i < picsArray.length; i++) {
				const pic = picsArray[i];
				expect(pic instanceof Picture).toBe(true);
				expect(bbox.contains(pic.coordinates)).toBe(true);
				expect(pic.provider == "Mapillary" || pic.provider == "Wikimedia Commons").toBe(true);
				if(pic.provider == "Mapillary") { mapillaryPics++; }
			}

			expect(mapillaryPics > 0).toBe(true);
		});
	}, REQUEST_TIMEOUT);

	it("over area with duplicated pictures", () => {
		const bbox = new LatLngBounds(new LatLng(47.265,-2.345), new LatLng(47.27,-2.34));
		const dm1 = new PicturesManager();

		return dm1.startPicsRetrieval(bbox, { mindate: 1492128000000, maxdate: 1492214400000 })
		.then(picsArray => {
			expect(picsArray.length > 0).toBe(true);

			picsArray.sort((a,b) => { return b.date - a.date; });

			for(let i=1; i < picsArray.length; i++) {
				expect(!picsArray[i].lookAlike(picsArray[i-1])).toBe(true);
			}
		});
	}, REQUEST_TIMEOUT);

	it("works with user defined fetchers", function() {
		global.fetch = csvFetcher;

		const bbox = new LatLngBounds(new LatLng(0,0), new LatLng(0.01,0.01));
		const dm1 = new PicturesManager({
			userDefinedFetchers: {
				f1: {
					csv: "http://myserver.net/csv_fetcher_1.csv",
					name: "F1"
				}
			}
		});

		return dm1.startPicsRetrieval(bbox, { usefetchers: [ "f1" ] })
		.then(picsArray => {
			expect(picsArray.length > 0).toBe(true);

			for(let i=0; i < picsArray.length; i++) {
				const pic = picsArray[i];
				expect(pic instanceof Picture).toBe(true);
				expect(bbox.contains(pic.coordinates)).toBe(true);
				expect(pic.date > 0).toBe(true);
				expect(pic.provider).toEqual("F1");
			}
		});
	}, REQUEST_TIMEOUT);

	it("sends events for watching fetchers progress", function() {
		const bbox = new LatLngBounds(new LatLng(47.265,-2.345), new LatLng(47.27,-2.34));
		const dm1 = new PicturesManager({
			userDefinedFetchers: {
				f1: {
					csv: "http://myserver.net/csv_fetcher_1.csv", //Server not launched for testing fetcher failure
					name: "F1"
				}
			}
		});

		let fetcherok = 0;
		let fetcherfail = 0;

		dm1.on("fetcherdone", fid => {
			fetcherok++;
		});

		dm1.on("fetcherfailed", fid => {
			fetcherfail++;
		});

		return dm1.startPicsRetrieval(bbox, { usefetchers: [ "mapillary", "f1" ] })
		.then(picsArray => {
			return new Promise((resolve) => {
				setTimeout(() => {
					expect(picsArray.length > 0).toBe(true);
					expect(fetcherok).toEqual(1);
					expect(fetcherfail).toEqual(1);
					resolve();
				}, 1000);
			});
		});
	}, REQUEST_TIMEOUT);

	it("uses globally defined options", () => {
		const bbox = new LatLngBounds(new LatLng(48.10,-1.69), new LatLng(48.105,-1.685));
		const fetchers = [ "flickr", "wikicommons" ];
		const dm1 = new PicturesManager({ usefetchers: fetchers });

		return dm1.startPicsRetrieval(bbox)
		.then(picsArray => {
			expect(picsArray.length > 0).toBe(true);

			for(let i=0; i < picsArray.length; i++) {
				const pic = picsArray[i];
				expect(pic instanceof Picture).toBe(true);
				expect(bbox.contains(pic.coordinates)).toBe(true);
				expect(pic.provider == "Flickr" || pic.provider == "Wikimedia Commons").toBe(true);
			}
		});
	}, REQUEST_TIMEOUT);
});

describe("startPicsRetrievalAround", function() {
	it("over not empty area", () => {
		const center = new LatLng(48.1306333, -1.6770553);
		const bbox = new LatLngBounds(new LatLng(48.130539, -1.677209), new LatLng(48.130722, -1.676919));
		const dm1 = new PicturesManager();

		return dm1.startPicsRetrievalAround(center, 10, { mindate: 1451606400000 })
		.then(picsArray => {
			expect(picsArray.length > 0).toBe(true);

			for(let i=0; i < picsArray.length; i++) {
				const pic = picsArray[i];
				expect(pic instanceof Picture).toBe(true);
				expect(bbox.contains(pic.coordinates)).toBe(true);
				expect(pic.date > 0).toBe(true);
			}


		});
	}, REQUEST_TIMEOUT);

	it("works over not empty area and looking to center", () => {
		const center = new LatLng(48.1306333, -1.6770553);
		const bbox = new LatLngBounds(new LatLng(48.130539, -1.677209), new LatLng(48.130722, -1.676919));
		const dm1 = new PicturesManager();

		return dm1.startPicsRetrievalAround(center, 10, { mindate: 1451606400000, towardscenter: true })
		.then(picsArray => {
			expect(picsArray.length > 0).toBe(true);

			for(let i=0; i < picsArray.length; i++) {
				const pic = picsArray[i];
				expect(pic instanceof Picture).toBe(true);
				expect(bbox.contains(pic.coordinates)).toBe(true);
				expect(pic.date > 0).toBe(true);
				expect(dm1._canBeSeen(pic.coordinates, center, pic.direction, 70)).toBe(true);
			}
		});
	}, REQUEST_TIMEOUT);

	it("works over not empty area and looking to center for 360° pictures", () => {
		const center = new LatLng(48.11620936351213, -1.686498639686647);
		const bbox = new LatLngBounds(new LatLng(48.1161, -1.6866), new LatLng(48.1163, -1.6849));
		const dm1 = new PicturesManager();

		return dm1.startPicsRetrievalAround(center, 10, { mindate: 1493769600000, maxdate: 1493855999000, towardscenter: true })
		.then(picsArray => {
			expect(picsArray.length > 0).toBe(true);

			for(let i=0; i < picsArray.length; i++) {
				const pic = picsArray[i];
				expect(pic instanceof Picture).toBe(true);
				expect(bbox.contains(pic.coordinates)).toBe(true);
				expect(pic.date > 0).toBe(true);
				expect(pic.details.isSpherical || dm1._canBeSeen(pic.coordinates, center, pic.direction, 70)).toBe(true);
			}
		});
	}, REQUEST_TIMEOUT);
});

describe("startSummaryRetrieval", function() {
	it("over not empty area", () => {
		const bbox = new LatLngBounds(new LatLng(48.10,-1.69), new LatLng(48.105,-1.685));
		const dm1 = new PicturesManager();

		return dm1.startSummaryRetrieval(bbox)
		.then(summary => {
			expect(summary.amount > 0).toBe(true);
			expect(summary.approxAmount === true || summary.approxAmount === false).toBe(true);
			expect(summary.last > 0).toBe(true);
		});
	}, REQUEST_TIMEOUT);

	it("over empty area", () => {
		const bbox = new LatLngBounds(new LatLng(35.75, -44.78), new LatLng(35.76, -44.77));
		const dm1 = new PicturesManager();

		return dm1.startSummaryRetrieval(bbox)
		.then(summary => {
			expect(summary.amount).toEqual(0);
			expect(!summary.approxAmount).toBe(true);
			expect(summary.last).toEqual(0);
		});
	}, REQUEST_TIMEOUT);

	it("over not empty area with options", () => {
		const bbox = new LatLngBounds(new LatLng(48.10,-1.69), new LatLng(48.105,-1.685));
		const dm1 = new PicturesManager();

		return dm1.startSummaryRetrieval(bbox)
		.then(summary => {
			expect(summary.amount > 0).toBe(true);
			expect(summary.approxAmount === true || summary.approxAmount === false).toBe(true);
			expect(summary.last > 0).toBe(true);

			return dm1.startSummaryRetrieval(bbox, { mindate: 1467377425000, maxdate: 1475326225000 })
			.then(summary2 => {
				expect(summary2.amount < summary.amount).toBe(true);
				expect(summary2.approxAmount === true || summary2.approxAmount === false).toBe(true);
				expect(summary2.last >= 1467377425000).toBe(true);
				expect(summary2.last <= 1475326225000).toBe(true);
			});
		});
	}, REQUEST_TIMEOUT);

	it("over not empty area with options and ignore fetchers", () => {
		const bbox = new LatLngBounds(new LatLng(48.13,-1.67), new LatLng(48.135,-1.665));
		const dm1 = new PicturesManager();

		return dm1.startSummaryRetrieval(bbox, { mindate: 1501545600000, maxdate: 1501891200000, ignorefetchers: [ "mapillary" ] })
		.then(summary => {
			expect(summary.amount).toEqual(0);
			expect(summary.approxAmount === false).toBe(true);
			expect(summary.last).toEqual(0);
		});
	}, REQUEST_TIMEOUT);

	it("over empty Paris in January 2017", () => {
		const bbox = new LatLngBounds(new LatLng(48.84,2.305), new LatLng(48.845,2.31));
		const dm1 = new PicturesManager();

		return dm1.startSummaryRetrieval(bbox, { mindate: 1483228800000, maxdate: 1484870400000 })
		.then(summary => {
			expect(summary.amount).toEqual(0);
			expect(!summary.approxAmount).toBe(true);
			expect(summary.last).toEqual(0);
		});
	}, REQUEST_TIMEOUT);

	it("over empty area in Cuba", () => {
		const bbox = new LatLngBounds(new LatLng(23.155,-81.24), new LatLng(23.16,-81.235));
		const dm1 = new PicturesManager();

		return dm1.startSummaryRetrieval(bbox, { mindate: Date.now()-1000*60*60*24*30*6 })
		.then(summary => {
			expect(summary.amount).toEqual(0);
			expect(!summary.approxAmount).toBe(true);
			expect(summary.last).toEqual(0);
		});
	}, REQUEST_TIMEOUT);

	it("sends events for watching fetchers progress", function() {
		const bbox = new LatLngBounds(new LatLng(48.13,-1.67), new LatLng(48.135,-1.665));
		const dm1 = new PicturesManager({
			userDefinedFetchers: {
				f1: {
					csv: "http://myserver.net/csv_fetcher_1.csv", //Server not launched for testing fetcher failure
					name: "F1"
				}
			}
		});

		let fetcherok = 0;
		let fetcherfail = 0;

		dm1.on("fetcherdone", fid => {
			fetcherok++;
		});

		dm1.on("fetcherfailed", fid => {
			fetcherfail++;
		});

		return dm1.startSummaryRetrieval(bbox, { mindate: 1501545600000, maxdate: 1501891200000, usefetchers: [ "mapillary", "f1" ] })
		.then(summary => {
			return new Promise((resolve) => {
				setTimeout(() => {
					expect(fetcherok).toEqual(1);
					expect(fetcherfail).toEqual(1);
					resolve();
				}, 1000);
			});
		});
	}, REQUEST_TIMEOUT);
});

describe("startDetectionsRetrieval", () => {
	it("works over not empty area", () => {
		const bbox = new LatLngBounds(new LatLng(48.12902, -1.67708), new LatLng(48.13101, -1.67515));
		const dm1 = new PicturesManager();

		return dm1.startDetectionsRetrieval(bbox, { types: [ Detection.SIGN_STOP ] })
		.then(features => {
			expect(features.length > 0).toBe(true);
			features.forEach(f => {
				expect(f.provider).toEqual("mapillary");
				expect(f.date).toBeDefined();
				expect(f.coordinates).toBeDefined();
				expect(f.type >= 0).toBe(true);
			});
		});
	}, REQUEST_TIMEOUT);

	it("works over not empty area and result as GeoJSON", () => {
		const bbox = new LatLngBounds(new LatLng(48.12902, -1.67708), new LatLng(48.13101, -1.67515));
		const dm1 = new PicturesManager();

		return dm1.startDetectionsRetrieval(bbox, { types: [ Detection.SIGN_STOP ], asgeojson: true })
		.then(geojson => {
			expect(geojson.type).toEqual("FeatureCollection");
			expect(geojson.features.length > 0).toBe(true);
			geojson.features.forEach(f => {
				expect(f.geometry.type).toEqual("Point");
				expect(f.geometry.coordinates.length).toEqual(2);
				expect(Object.keys(f.properties).length > 0).toBe(true);
			});
		});
	}, REQUEST_TIMEOUT);
});

describe("getPicturesFromTags", () => {
	it("works", () => {
		const dm1 = new PicturesManager();
		const tags = {
			"amenity": "place_of_worship",
			"building": "church",
			"denomination": "catholic",
			"description": "Edificio in pietra, riedificato nel 1709. La chiesa primitiva, del 1200, fu distrutta da un fulmine.",
			"historic": "church",
			"image": "File:Chiesetta_di_San_Sfirio.jpg",
			"mapillary": "PnaVMtVwaIWiWRp4-EFzYQ",
			"name": "Chiesetta di San Sfirio",
			"religion": "christian",
			"source": "Database topografico regione Lombardia",
			"start_date": "1709",
			"wikimedia_commons": "File:Chiesetta_di_San_Sfirio.jpg"
		};

		const result = dm1.getPicturesFromTags(tags);
		expect(result.length).toEqual(2);
		expect(result[0]).toEqual("https://images.mapillary.com/PnaVMtVwaIWiWRp4-EFzYQ/thumb-2048.jpg");
		expect(result[1]).toEqual("https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Chiesetta_di_San_Sfirio.jpg/1024px-Chiesetta_di_San_Sfirio.jpg");
	});
});

describe("_canBeSeen", () => {
	it("works on 0,0 N", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(0,0), new LatLng(1,0), 0, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(0,0), new LatLng(1,0), 180, 10);
		expect(!result2).toBe(true);
	});

	it("works on 0,0 NW", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(0,0), new LatLng(1,-1), 315, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(0,0), new LatLng(1,-1), 135, 10);
		expect(!result2).toBe(true);
	});

	it("works on 0,0 W", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(0,0), new LatLng(0,-1), 270, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(0,0), new LatLng(0,-1), 90, 10);
		expect(!result2).toBe(true);
	});

	it("works on 0,0 SW", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(0,0), new LatLng(-1,-1), 225, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(0,0), new LatLng(-1,-1), 45, 10);
		expect(!result2).toBe(true);
	});

	it("works on 0,0 S", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(0,0), new LatLng(-1,0), 180, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(0,0), new LatLng(-1,0), 0, 10);
		expect(!result2).toBe(true);
	});

	it("works on 0,0 SE", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(0,0), new LatLng(-1,1), 135, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(0,0), new LatLng(-1,1), 315, 10);
		expect(!result2).toBe(true);
	});

	it("works on 0,0 E", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(0,0), new LatLng(0,1), 90, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(0,0), new LatLng(0,1), 270, 10);
		expect(!result2).toBe(true);
	});

	it("works on 0,0 NE", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(0,0), new LatLng(1,1), 45, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(0,0), new LatLng(1,1), 225, 10);
		expect(!result2).toBe(true);
	});

	//
	it("works on 1,1 N", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(1,1), new LatLng(2,1), 0, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(1,1), new LatLng(2,1), 180, 10);
		expect(!result2).toBe(true);
	});

	it("works on 1,1 NW", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(1,1), new LatLng(2,0), 315, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(1,1), new LatLng(2,0), 135, 10);
		expect(!result2).toBe(true);
	});

	it("works on 1,1 W", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(1,1), new LatLng(1,0), 270, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(1,1), new LatLng(1,0), 90, 10);
		expect(!result2).toBe(true);
	});

	it("works on 1,1 SW", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(1,1), new LatLng(0,0), 225, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(1,1), new LatLng(0,0), 45, 10);
		expect(!result2).toBe(true);
	});

	it("works on 1,1 S", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(1,1), new LatLng(0,1), 180, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(1,1), new LatLng(0,1), 0, 10);
		expect(!result2).toBe(true);
	});

	it("works on 1,1 SE", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(1,1), new LatLng(0,2), 135, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(1,1), new LatLng(0,2), 315, 10);
		expect(!result2).toBe(true);
	});

	it("works on 1,1 E", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(1,1), new LatLng(1,2), 90, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(1,1), new LatLng(1,2), 270, 10);
		expect(!result2).toBe(true);
	});

	it("works on 1,1 NE", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(1,1), new LatLng(2,2), 45, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(1,1), new LatLng(2,2), 225, 10);
		expect(!result2).toBe(true);
	});

	//
	it("works on -1,-1 N", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(0,-1), 0, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(0,-1), 180, 10);
		expect(!result2).toBe(true);
	});

	it("works on -1,-1 NW", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(0,-2), 315, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(0,-2), 135, 10);
		expect(!result2).toBe(true);
	});

	it("works on -1,-1 W", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(-1,-2), 270, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(-1,-2), 90, 10);
		expect(!result2).toBe(true);
	});

	it("works on -1,-1 SW", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(-2,-2), 225, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(-2,-2), 45, 10);
		expect(!result2).toBe(true);
	});

	it("works on -1,-1 S", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(-2,-1), 180, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(-2,-1), 0, 10);
		expect(!result2).toBe(true);
	});

	it("works on -1,-1 SE", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(-2,0), 135, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(-2,0), 315, 10);
		expect(!result2).toBe(true);
	});

	it("works on -1,-1 E", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(-1,0), 90, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(-1,0), 270, 10);
		expect(!result2).toBe(true);
	});

	it("works on -1,-1 NE", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(0,0), 45, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(0,0), 225, 10);
		expect(!result2).toBe(true);
	});

	//
	it("works on -1,0 N", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(0,0), 0, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(0,0), 180, 10);
		expect(!result2).toBe(true);
	});

	it("works on -1,0 NW", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(0,-1), 315, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(0,-1), 135, 10);
		expect(!result2).toBe(true);
	});

	it("works on -1,0 W", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(-1,-1), 270, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(-1,-1), 90, 10);
		expect(!result2).toBe(true);
	});

	it("works on -1,0 SW", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(-2,-1), 225, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(-2,-1), 45, 10);
		expect(!result2).toBe(true);
	});

	it("works on -1,0 S", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(-2,0), 180, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(-2,0), 0, 10);
		expect(!result2).toBe(true);
	});

	it("works on -1,0 SE", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(-2,1), 135, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(-2,1), 315, 10);
		expect(!result2).toBe(true);
	});

	it("works on -1,0 E", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(-1,1), 90, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(-1,1), 270, 10);
		expect(!result2).toBe(true);
	});

	it("works on -1,0 NE", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(0,1), 45, 10);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(0,1), 225, 10);
		expect(!result2).toBe(true);
	});

	it("works on Rennes", () => {
		const dm1 = new PicturesManager();
		const result1 = dm1._canBeSeen(new LatLng(48.11397,-1.68043), new LatLng(48.11377,-1.68072), 225, 100);
		expect(result1).toBe(true);

		const result2 = dm1._canBeSeen(new LatLng(48.11397,-1.68043), new LatLng(48.11447,-1.67925), 225, 100);
		expect(!result2).toBe(true);
	});
});
