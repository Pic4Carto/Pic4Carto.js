/**
 * Test script for EventLauncher
 */

import EventLauncher from "../../src/lib/ctrl/EventLauncher";

class Subclass extends EventLauncher {
	constructor() {
		super();
	}
}

describe("Constructor", function() {
	it("Constructor which can't be instantiated", () => {
		expect(() => new EventLauncher()).toThrow("ctrl.eventlauncher.cantinstantiate");
	});

	it("Constructor of heriting class", () => {
		expect(new Subclass()).toBeTruthy();
		
		const sc1 = new Subclass();
		expect(sc1.fire).toBeTruthy();
		expect(sc1.on).toBeTruthy();
		expect(sc1.off).toBeTruthy();
	});
});

describe("fire", function() {
	it("fire with associated object", () => {
		const sc1 = new Subclass();
		
		return new Promise(resolve => {
			sc1.on("testfire", (o) => {
				expect(o.defined).toBeTruthy();
				resolve();
			});
			sc1.fire("testfire", { defined: true });
		})
	});
});

describe("on", function() {
	it("on with multiple listeners", () => {
		const sc1 = new Subclass();
		const max = 2;
		let i = 0;
		
		return new Promise(resolve => {
			sc1.on("testfire", (o) => {
				expect(o.defined).toBeTruthy();
				i++;
				if(i == max) {
					resolve();
				}
			});
			
			sc1.on("testfire", (o) => {
				expect(o.test2).toBeTruthy();
				i++;
				if(i == max) {
					resolve();
				}
			});
			
			sc1.fire("testfire", { defined: true, test2: true });
		});
	});
});

describe("off", function() {
	it("off on one listener", () => {
		const sc1 = new Subclass();
		const max = 3;
		let i = 0;
		
		return new Promise(resolve => {
			const l1 = (o) => {
				expect(o.defined).toBeTruthy();
				i++;
				if(i == max) {
					resolve();
				}
			};
			const l2 = (o) => {
				expect(o.test2).toBeTruthy();
				i++;
				if(i == max) {
					resolve();
				}
			};
			
			sc1.on("testfire", l1);
			sc1.on("testfire", l2);
			
			sc1.fire("testfire", { defined: true, test2: true });
			
			setTimeout(
				() => {
					sc1.off("testfire", l1);
					sc1.fire("testfire", { defined: true, test2: true });
				},
				100
			);
		});
	});

	it("off on all listeners", () => {
		const sc1 = new Subclass();
		
		return new Promise(resolve => {
			const l1 = (o) => {
				expect(false).toBeTruthy();
				resolve();
			};
			const l2 = (o) => {
				expect(false).toBeTruthy();
				resolve();
			};
			
			sc1.on("testfire", l1);
			sc1.on("testfire", l2);
			
			setTimeout(
				() => {
					sc1.off("testfire");
					sc1.fire("testfire", { defined: true, test2: true });
					setTimeout(() => {
							expect(true).toBeTruthy();
							resolve();
						},
						200
					);
				},
				100
			);
		});
	});
});

it("supports chained calls", () => {
	const sc1 = new Subclass();
	
	return new Promise(resolve => {
		sc1.on("testfire", (o) => {
			expect(o.defined).toBeTruthy();
			resolve();
		})
		.fire("testfire", { defined: true });
	});
});
