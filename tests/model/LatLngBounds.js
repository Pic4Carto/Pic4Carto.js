/*
 * Test script for model/LatLng.js
 */

import LatLng from "../../src/lib/model/LatLng";
import LatLngBounds from "../../src/lib/model/LatLngBounds";
const that = {};

beforeEach(() => {
	that.a = new LatLngBounds(
		new LatLng(14, 12),
		new LatLng(30, 40));
	that.c = new LatLngBounds();
});

it('instantiates either passing two latlngs or an array of latlngs', () => {
	var b = new LatLngBounds([
		new LatLng(14, 12),
		new LatLng(30, 40)
	]);
	expect(b.equals(that.a)).toBe(true);
	expect(b.getNorthWest().equals(new LatLng(30, 12))).toBe(true);
});

it('returns an empty bounds when not argument is given', () => {
	var bounds = new LatLngBounds();
	expect(bounds instanceof LatLngBounds).toBe(true);
});

it('extends the bounds by a given point', () => {
	that.a.extend(new LatLng(20, 50));
	expect(that.a.getNorthEast().equals(new LatLng(30, 50))).toBe(true);
});

it('extends the bounds by undefined', () => {
	expect(that.a.extend().equals(that.a)).toBe(true);
});

it('extends the bounds by raw object', () => {
	that.a.extend({lat: 20, lng: 50});
	expect(that.a.getNorthEast().equals(new LatLng(30, 50))).toBe(true);
});

it('extend the bounds by an empty bounds object', () => {
	expect(that.a.extend(new LatLngBounds()).equals(that.a)).toBe(true);
});

it('returns the bounds center', () => {
	expect(that.a.getCenter().equals(new LatLng(22, 26))).toBe(true);
});

it('pads the bounds by a given ratio', () => {
	var b = that.a.pad(0.5);

	expect(b.equals(new LatLngBounds([[6, -2], [38, 54]]))).toBe(true);
});

it('returns true if bounds equal', () => {
	expect(that.a.equals([[14, 12], [30, 40]])).toBe(true);
	expect(!that.a.equals([[14, 13], [30, 40]])).toBe(true);
	expect(!that.a.equals(null)).toBe(true);
});

it('returns true if properly set up', () => {
	expect(that.a.isValid()).toBe(true);
});
it('returns false if is invalid', () => {
	expect(!that.c.isValid()).toBe(true);
});
it('returns true if extended', () => {
	that.c.extend([0, 0]);
	expect(that.c.isValid()).toBe(true);
});

it('returns a proper bbox west value', () => {
	expect(that.a.getWest()).toEqual(12);
});

it('returns a proper bbox south value', () => {
	expect(that.a.getSouth()).toEqual(14);
});

it('returns a proper bbox east value', () => {
	expect(that.a.getEast()).toEqual(40);
});

it('returns a proper bbox north value', () => {
	expect(that.a.getNorth()).toEqual(30);
});

it('returns a proper left,bottom,right,top bbox', () => {
	expect(that.a.toBBoxString()).toEqual("12,14,40,30");
});

it('returns a proper north-west LatLng', () => {
	expect(that.a.getNorthWest().equals(new LatLng(that.a.getNorth(), that.a.getWest()))).toBe(true);
});

it('returns a proper south-east LatLng', () => {
	expect(that.a.getSouthEast().equals(new LatLng(that.a.getSouth(), that.a.getEast()))).toBe(true);
});

it('returns true if contains latlng point as array', () => {
	expect(that.a.contains([16, 20])).toBe(true);
	expect(!new LatLngBounds(that.a).contains([5, 20])).toBe(true);
});

it('returns true if contains latlng point as {lat:, lng:} object', () => {
	expect(that.a.contains({lat: 16, lng: 20})).toBe(true);
	expect(!new LatLngBounds(that.a).contains({lat: 5, lng: 20})).toBe(true);
});

it('returns true if contains latlng point as LatLng instance', () => {
	expect(that.a.contains(new LatLng([16, 20]))).toBe(true);
	expect(!new LatLngBounds(that.a).contains(new LatLng([5, 20]))).toBe(true);
});

it('returns true if contains bounds', () => {
	expect(that.a.contains([[16, 20], [20, 40]])).toBe(true);
	expect(!that.a.contains([[16, 50], [8, 40]])).toBe(true);
});

it('returns true if intersects the given bounds', () => {
	expect(that.a.intersects([[16, 20], [50, 60]])).toBe(true);
	expect(!that.a.contains([[40, 50], [50, 60]])).toBe(true);
});

it('returns true if just touches the boundary of the given bounds', () => {
	expect(that.a.intersects([[25, 40], [55, 50]])).toBe(true);
});

it('returns true if overlaps the given bounds', () => {
	expect(that.a.overlaps([[16, 20], [50, 60]])).toBe(true);
});
it('returns false if just touches the boundary of the given bounds', () => {
	expect(!that.a.overlaps([[25, 40], [55, 50]])).toBe(true);
});
