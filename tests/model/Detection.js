/*
 * Test script for model/Detection.js
 */

import LatLng from "../../src/lib/model/LatLng";
import Detection from "../../src/lib/model/Detection";

describe("Constructor", function() {
	it("handles valid parameters", function() {
		const d1 = new Detection(Detection.SIGN_STOP, new LatLng(47.3, -1.8), 123456, "mapillary");
		expect(d1).toBeDefined();
		expect(d1.type).toBe(Detection.SIGN_STOP);
		expect(d1.coordinates.lat).toBe(47.3);
		expect(d1.coordinates.lng).toBe(-1.8);
		expect(d1.date).toBe(123456);
		expect(d1.provider).toBe("mapillary");
	});
	
	it("fails if missing params", () => {
		expect(() => new Detection()).toThrow("model.detection.invalid.type");
		expect(() => new Detection(Detection.SIGN_STOP)).toThrow("model.detection.invalid.coordinates");
		expect(() => new Detection(Detection.SIGN_STOP, new LatLng(47.3, -1.8))).toThrow("model.detection.invalid.date");
		expect(() => new Detection(Detection.SIGN_STOP, new LatLng(47.3, -1.8), 123456)).toThrow("model.detection.invalid.provider");
	});
});
