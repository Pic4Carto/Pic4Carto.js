/*
 * Test script for model/Picture.js
 */

import LatLng from "../../src/lib/model/LatLng";
import Picture from "../../src/lib/model/Picture";

describe("Constructor", function() {
	it("handles valid parameters", function() {
		const p1 = new Picture("http://test.net/img.jpg", 123456789, new LatLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details.html", 175, { image: "https://image.net" },"http://test.net/img_small.jpg", { isSpherical: true });
		
		//Check attributes
		expect(p1.pictureUrl).toEqual("http://test.net/img.jpg");
		expect(p1.date).toEqual(123456789);
		expect(p1.coordinates.equals(new LatLng(10.1, 48.7))).toBe(true);
		expect(p1.provider).toEqual("Myself");
		expect(p1.author).toEqual("Me");
		expect(p1.license).toEqual("My license");
		expect(p1.detailsUrl).toEqual("http://test.net/img_details.html");
		expect(p1.direction).toEqual(175);
		expect(p1.osmTags.image).toEqual("https://image.net");
		expect(p1.thumbUrl).toEqual("http://test.net/img_small.jpg");
		expect(p1.details.isSpherical).toBe(true);
	});

	it("handles valid parameters (only mandatory ones)", function() {
		const p1 = new Picture("http://test.net/img.jpg", 123456789, new LatLng(10.1, 48.7), "Myself");
		
		//Check attributes
		expect(p1.pictureUrl).toEqual("http://test.net/img.jpg");
		expect(p1.date).toEqual(123456789);
		expect(p1.coordinates.equals(new LatLng(10.1, 48.7))).toBe(true);
		expect(p1.provider).toEqual("Myself");
		expect(p1.author).toBeNull();
		expect(p1.license).toBeNull();
		expect(p1.detailsUrl).toBeNull();
		expect(p1.direction).toBeNull();
		expect(p1.osmTags).toBeNull();
		expect(p1.thumbUrl).toBeNull();
		expect(Object.keys(p1.details).length).toEqual(0);
	});

	it("handles missing mandatory parameters", function() {
		expect(() => new Picture()).toThrow("model.picture.invalid.pictureUrl");
		expect(() => new Picture("http://test.net/img.jpg")).toThrow("model.picture.invalid.date");
		expect(() => new Picture("http://test.net/img.jpg", 123456789)).toThrow("model.picture.invalid.coords");
		expect(() => new Picture("http://test.net/img.jpg", 123456789, new LatLng(10, 20))).toThrow("model.picture.invalid.provider");
	});

	it("Constructor with invalid parameters types", function() {
		expect(() => new Picture(123456, 123456789, new LatLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details.html", 175)).toThrow("model.picture.invalid.pictureUrl");
		expect(() => new Picture("http://test.net/img.jpg", "lol", new LatLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details.html", 175)).toThrow("model.picture.invalid.date");
		expect(() => new Picture("http://test.net/img.jpg", 123456789, 42, "Myself", "Me", "My license", "http://test.net/img_details.html", 175)).toThrow("model.picture.invalid.coords");
		expect(() => new Picture("http://test.net/img.jpg", 123456789, new LatLng(10.1, 48.7), 42, "Me", "My license", "http://test.net/img_details.html", 175)).toThrow("model.picture.invalid.provider");
		const p1 = new Picture("http://test.net/img.jpg", 123456789, new LatLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details.html", "north");
		expect(p1.direction).toBeNull();
	});

	it("Constructor with negative timestamp", function() {
		const p1 = new Picture("http://test.net/img.jpg", -123456789, new LatLng(10.1, 48.7), "Myself");
		
		//Check attributes
		expect(p1.pictureUrl).toEqual("http://test.net/img.jpg");
		expect(p1.date).toEqual(-123456789);
		expect(p1.coordinates.equals(new LatLng(10.1, 48.7))).toBe(true);
		expect(p1.provider).toEqual("Myself");
		expect(p1.author).toBeNull();
		expect(p1.license).toBeNull();
		expect(p1.detailsUrl).toBeNull();
		expect(p1.direction).toBeNull();
	});
});

describe("lookAlike", function() {
	it("lookAlike with similar pictures", function() {
		const p1 = new Picture("http://provider1.net/img1.jpg", 123456, new LatLng(1, 2), "Provider1", "Author1", "License1", "http://provider1.net/details/img1", 200);
		const p2 = new Picture("http://provider2.net/img2048.jpg", 123456, new LatLng(1, 2), "Provider2", "Author2", "License2", "http://provider2.net/see/img2048", 200);
		
		expect(p1.lookAlike(p2)).toBe(true);
	});

	it("lookAlike with distinct pictures", function() {
		const p1 = new Picture("http://provider1.net/img1.jpg", 123789, new LatLng(2, 2), "Provider1", "Author1", "License1", "http://provider1.net/details/img1", 125);
		const p2 = new Picture("http://provider2.net/img2048.jpg", 123456, new LatLng(3, 4), "Provider2", "Author2", "License2", "http://provider2.net/see/img2048", 200);
		
		expect(!p1.lookAlike(p2)).toBe(true);
	});
});
