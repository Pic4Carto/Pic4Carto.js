/*
 * Test script for model/LatLng.js
 */

import LatLng from "../../src/lib/model/LatLng";

describe("Constructor", function() {
	it("sets lat and lng", () => {
		const a = new LatLng(25, 74);
		expect(a.lat).toEqual(25);
		expect(a.lng).toEqual(74);
		
		const b = new LatLng(-25, -74);
		expect(b.lat).toEqual(-25);
		expect(b.lng).toEqual(-74);
	});

	it('throws an error if invalid lat or lng', () => {
		expect(() => new LatLng(NaN, NaN)).toThrow("Invalid LatLng object: (NaN, NaN)");
	});

	it('does not set altitude if undefined', () => {
		const a = new LatLng(25, 74);
		expect(a.alt).toBeUndefined();
	});

	it('sets altitude', () => {
		const a = new LatLng(25, 74, 50);
		expect(a.alt).toEqual(50);
		
		const b = new LatLng(-25, -74, -50);
		expect(b.alt).toEqual(-50);
	});
});

describe("distanceTo", function() {
	it("calculates distance", () => {
		const p1 = new LatLng(36.12, -86.67);
		const p2 = new LatLng(33.94, -118.40);
		expect(p1.distanceTo(p2)).toBeGreaterThanOrEqual(2886444.43);
		expect(p1.distanceTo(p2)).toBeLessThanOrEqual(2886444.45);
	});
});
