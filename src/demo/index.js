import * as P4C from "../lib";
import "./index.css";
import exampleCsv from "./example.csv";

const pm = new P4C.PicturesManager({userDefinedFetchers: {
	mycsv: {csv: exampleCsv}
}});

//Call for pictures summary download (around Rennes center)
pm.startSummaryRetrieval(new P4C.LatLngBounds(new P4C.LatLng(48.1075, -1.6860), new P4C.LatLng(48.1156, -1.6739)))
.then(function(summary) {
	//Change text
	document.getElementById("summary").innerHTML = "Available pictures: "+((summary.approxAmount) ? "~" : "")+summary.amount+" | Last picture date: "+(new Date(summary.last).toDateString());
});

//Call for pictures download (around Rennes center, since mid-2016)
pm.startPicsRetrieval(new P4C.LatLngBounds(new P4C.LatLng(48.1075, -1.6860), new P4C.LatLng(48.1156, -1.6739)), { mindate: 1464775200000 })
.then(function(pictures) {
	for(var i=0; i < pictures.length && i < 15; i++) {
		var randomId = Math.floor(Math.random() * pictures.length);
		var pic = pictures[randomId]; //Random image
		
		//Define img in DOM
		var img = document.createElement("img");
		img.src = pic.pictureUrl;
		img.title = pic.author + " - " + (new Date(pic.date).toDateString()) + " ("+pic.provider+")";
		
		//Append to div
		document.getElementById("pictures").appendChild(img);
	}
	
	//Remove loading message
	document.getElementById("loading").parentNode.removeChild(document.getElementById("loading"));
});

//Call for pictures download (around Orange, since mid-2016)
pm.startPicsRetrievalAround(new P4C.LatLng(44.1355674, 4.8072052), 20, { mindate: 1420070400000, towardscenter: true })
.then(function(pictures) {
	for(var i=0; i < pictures.length; i++) {
		var pic = pictures[i];
		
		//Define img in DOM
		var link = document.createElement("a");
		link.href = pic.detailsUrl;
		link.target = "_blank";
		
		var img = document.createElement("img");
		img.src = pic.pictureUrl;
		img.title = pic.author + " - " + (new Date(pic.date).toDateString()) + " ("+pic.provider+")";
		link.appendChild(img);
		
		//Append to div
		document.getElementById("pictures2").appendChild(link);
	}
	
	//Remove loading message
	document.getElementById("loading2").parentNode.removeChild(document.getElementById("loading2"));
});

//Call for pictures download (from CSV)
pm.startPicsRetrievalAround(
	new P4C.LatLng(48.33726, -1.933143),
	100,
	{ usefetchers: ["mycsv"] }
).then(function(pictures) {
	pictures.forEach(pic => {
		//Define img in DOM
		var img = document.createElement("img");
		img.src = pic.pictureUrl;
		img.title = pic.author + " - " + (new Date(pic.date).toDateString()) + " ("+pic.provider+")";
		
		//Append to div
		document.getElementById("pictures3").appendChild(img);
	});
	
	//Remove loading message
	document.getElementById("loading3").parentNode.removeChild(document.getElementById("loading3"));
});

document.querySelector("body").innerHTML = `
	<h1>Pic4Carto.js</h1>
	<h2>Summary: pictures over Rennes center</h2>
	<p id="summary">Loading...</p>

	<h2>List: pictures over Rennes</h2>
	<p id="loading">Loading...</p>
	<div class="pictures" id="pictures"></div>

	<h2>Around: bus stop in Orange</h2>
	<p><a href="https://www.openstreetmap.org/node/1300761015" target="_blank">This one</a> in particular</p>
	<p id="loading2">Loading...</p>
	<div class="pictures" id="pictures2"></div>

	<h2>CSV custom fetcher</h2>
	<p id="loading3">Loading...</p>
	<div class="pictures" id="pictures3"></div>
`;
