/**
 * Represents a geographical point with a certain latitude and longitude.
 * @example
 * var latlng = new LatLng(50.5, 30.5);
 * @see {@link https://github.com/Leaflet/Leaflet/blob/master/src/geo/LatLng.js}
 * @author Leaflet Team
 * @param {float} lat The latitude, in degrees.
 * @param {float} lng The longitude, in degrees.
 * @param {float} [alt] The altitude, in meters.
 */
export default class LatLng {
	constructor(lat, lng, alt) {
		// 	if (!lat instanceof LatLng && typeof lat !== "array" && (isNaN(lat) || isNaN(lng))) {
		// 		throw new Error('Invalid LatLng object: (' + lat + ', ' + lng + ')');
		// 	}
		if (!((lat instanceof LatLng || (lat && lat.length && lat.length == 2) || (lat.lat && lat.lng)) && lng === undefined)
			&& !(!isNaN(lat) && !isNaN(lng))) {
			throw new Error('Invalid LatLng object: (' + lat + ', ' + lng + ')');
		}

		if (lat instanceof LatLng || (lat.lat && lat.lng)) {
			this.lat = lat.lat;
			this.lng = lat.lng;
			this.alt = lat.alt;
		}
		else if (lat.length == 2) {
			this.lat = lat[0];
			this.lng = lat[1];
		}
		else {
			this.lat = +lat;

			this.lng = +lng;

			if (alt !== undefined) {
				this.alt = +alt;
			}
		}
	}
	// @method equals(otherLatLng: LatLng, maxMargin?: Number): Boolean
	// Returns `true` if the given `LatLng` point is at the same position (within a small margin of error). The margin of error can be overriden by setting `maxMargin` to a small number.
	equals(obj, maxMargin) {
		if (!obj) { return false; }

		obj = new LatLng(obj);

		var margin = Math.max(
			Math.abs(this.lat - obj.lat),
			Math.abs(this.lng - obj.lng));

		return margin <= (maxMargin === undefined ? 1.0E-9 : maxMargin);
	}
	// @method toString(): String
	// Returns a string representation of the point (for debugging purposes).
	toString(precision) {
		return 'LatLng(' +
			this.lat + ', ' +
			this.lng + ')';
	}
	// @method wrap(): LatLng
	// Returns a new `LatLng` object with the longitude wrapped so it's always between -180 and +180 degrees.
	wrap() {
		let lng = this.lng;
		while (lng < -180) { lng += 360; }
		while (lng > 180) { lng -= 360; }

		let lat = this.lat;
		while (lat < -90) { lat += 180; }
		while (lat > 90) { lat -= 180; }

		return new LatLng(lat, lng, this.alt);
	}
	// @method toBounds(sizeInMeters: Number): LatLngBounds
	// Returns a new `LatLngBounds` object in which each boundary is `sizeInMeters/2` meters apart from the `LatLng`.
	toBounds(sizeInMeters) {
		var latAccuracy = 180 * sizeInMeters / 40075017, lngAccuracy = latAccuracy / Math.cos((Math.PI / 180) * this.lat);

		return L.latLngBounds(
			[this.lat - latAccuracy, this.lng - lngAccuracy],
			[this.lat + latAccuracy, this.lng + lngAccuracy]);
	}
	clone() {
		return new LatLng(this.lat, this.lng, this.alt);
	}
	distanceTo(other) {
		var rad = Math.PI / 180, lat1 = this.lat * rad, lat2 = other.lat * rad, a = Math.sin(lat1) * Math.sin(lat2) +
			Math.cos(lat1) * Math.cos(lat2) * Math.cos((other.lng - this.lng) * rad);

		return 6371000 * Math.acos(Math.min(a, 1));
	}
}
