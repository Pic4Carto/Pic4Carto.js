/**
 * A detection is a geolocated feature extracted from analysis of provider pictures.
 * It has mainly a type (stop sign, crossing, hydrant...) and coordinates.
 * 
 * @param {int} type The kind of feature (use class constants)
 * @param {LatLng} coordinates The feature geolocation
 * @param {Date} date When the feature was detected, in milliseconds since 1st January 1970 (Unix time).
 * @param {string} provider The detection provider
 */
export default class Detection {
	constructor(type, coordinates, date, provider) {
		if(typeof type !== "number" || type < 0) {
			throw new Error("model.detection.invalid.type");
		}

		if(coordinates === null || coordinates === undefined || typeof coordinates.lat !== "number" || typeof coordinates.lng !== "number") {
			throw new Error("model.detection.invalid.coordinates");
		}

		if(typeof date === "number" && !isNaN(date)) {
			//Check format of date
			try {
				new Date(date);
			}
			catch(e) {
				throw new Error("model.detection.invalid.date");
			}
		}
		else {
			throw new Error("model.detection.invalid.date");
		}

		if(typeof provider !== "string" || provider.length === 0) {
			throw new Error("model.detection.invalid.provider");
		}

		/** The feature type (for example, stop sign, hydrant, bench...) **/
		this.type = type;

		/** The feature geolocation **/
		this.coordinates = coordinates;

		/** The date when the feature was detected **/
		this.date = date;

		/** The detection provider **/
		this.provider = provider;
	}
}


/*
 * List of feature types
 */
/** Bench type **/
Detection.OBJECT_BENCH = 1;
/** Stop sign type **/
Detection.SIGN_STOP = 2;
/** Pedestrian crossing type **/
Detection.MARK_CROSSING = 3;
/** Bicycle parking type **/
Detection.OBJECT_BICYCLE_PARKING = 4;
/** CCTV type **/
Detection.OBJECT_CCTV = 5;
/** Fire hydrant type **/
Detection.OBJECT_HYDRANT = 6;
/** Post box type **/
Detection.OBJECT_POSTBOX = 7;
/** Manhole type **/
Detection.OBJECT_MANHOLE = 8;
/** Parking meter type **/
Detection.OBJECT_PARKING_METER = 9;
/** Telephone type **/
Detection.OBJECT_PHONE = 10;
/** Advert sign type **/
Detection.SIGN_ADVERT = 11;
/** Information sign type **/
Detection.SIGN_INFO = 12;
/** Store sign type **/
Detection.SIGN_STORE = 13;
/** Street light type **/
Detection.OBJECT_STREET_LIGHT = 14;
/** Pole type **/
Detection.OBJECT_POLE = 15;
/** Utility pole type **/
Detection.OBJECT_UTILITY_POLE = 19;
/** Reserved parking type **/
Detection.SIGN_RESERVED_PARKING = 16;
/** Animal crossing (all kind) type **/
Detection.SIGN_ANIMAL_CROSSING = 17;
/** Railway crossing type **/
Detection.SIGN_RAILWAY_CROSSING = 18;

/**
 * Details for features types
 * Use Detection.* constants as key, which gives you { name, osmTags, symbol } object as value.
 */
Detection.TYPE_DETAILS = {
	[Detection.OBJECT_BENCH]: {
		name: "Bench",
		osmTags: { amenity: "bench" },
		symbol: "https://upload.wikimedia.org/wikipedia/commons/thumb/8/85/Garden_bench_001.jpg/320px-Garden_bench_001.jpg"
	},
	[Detection.SIGN_STOP]: {
		name: "Stop sign",
		osmTags: { traffic_sign: "stop" },
		symbol: "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1e/Vienna_Convention_road_sign_B2a.svg/320px-Vienna_Convention_road_sign_B2a.svg.png"
	},
	[Detection.MARK_CROSSING]: {
		name: "Pedestrian crossing",
		osmTags: { highway: "crossing" },
		symbol: "https://upload.wikimedia.org/wikipedia/commons/thumb/3/37/A_closeup_of_Pedastrian_cross.JPG/320px-A_closeup_of_Pedastrian_cross.JPG"
	},
	[Detection.OBJECT_BICYCLE_PARKING]: {
		name: "Bicycle parking",
		osmTags: { amenity: "bicycle_parking" },
		symbol: "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/AlewifeBikeParking.agr.2001.JPG/320px-AlewifeBikeParking.agr.2001.JPG"
	},
	[Detection.OBJECT_CCTV]: {
		name: "CCTV camera",
		osmTags: { "man_made": "surveillance" },
		symbol: "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/AxisCCTV.jpg/320px-AxisCCTV.jpg"
	},
	[Detection.OBJECT_HYDRANT]: {
		name: "Fire hydrant",
		osmTags: { emergency: "fire_hydrant", "fire_hydrant:type": "pillar" },
		symbol: "https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Brandkraan.jpg/320px-Brandkraan.jpg"
	},
	[Detection.OBJECT_POSTBOX]: {
		name: "Post box",
		osmTags: { amenity: "post_box" },
		symbol: "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/Mt_Abu_mailbox.jpg/320px-Mt_Abu_mailbox.jpg"
	},
	[Detection.OBJECT_MANHOLE]: {
		name: "Manhole",
		osmTags: { manhole: "unknown" },
		symbol: "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/French_Quater_New_Orleans_Curious_Lid_NIOPSI.jpg/320px-French_Quater_New_Orleans_Curious_Lid_NIOPSI.jpg"
	},
	[Detection.OBJECT_PARKING_METER]: {
		name: "Parking meter",
		osmTags: { amenity: "vending_machine", vending: "parking_tickets" },
		symbol: "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Riga_%2813.08.2011%29_405.JPG/320px-Riga_%2813.08.2011%29_405.JPG"
	},
	[Detection.OBJECT_PHONE]: {
		name: "Telephone (public/emergency)",
		osmTags: { amenity: "telephone" },
		symbol: "https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Telefono_publico_venezolano_2012_000.jpg/320px-Telefono_publico_venezolano_2012_000.jpg"
	},
	[Detection.SIGN_ADVERT]: {
		name: "Advert sign",
		osmTags: { advertising: "sign" },
		symbol: "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/The_Chevron_sign_of_excellence%2C_Chevron_Motoroils%2C_enamel_advertising_sign.JPG/320px-The_Chevron_sign_of_excellence%2C_Chevron_Motoroils%2C_enamel_advertising_sign.JPG"
	},
	[Detection.SIGN_INFO]: {
		name: "Information sign",
		osmTags: { advertising: "board" },
		symbol: "https://wiki.openstreetmap.org/w/images/thumb/f/fc/IMG_6076.JPG/320px-IMG_6076.JPG"
	},
	[Detection.SIGN_STORE]: {
		name: "Store sign",
		osmTags: { advertising: "sign" },
		symbol: "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/Kruidvat_store_sign%2C_Winschoten_%282019%29_01.jpg/320px-Kruidvat_store_sign%2C_Winschoten_%282019%29_01.jpg"
	},
	[Detection.OBJECT_STREET_LIGHT]: {
		name: "Street light",
		osmTags: { highway: "street_lamp" },
		symbol: "https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/Rome_(Italy)%2C_street_light_--_2013_--_3484.jpg/320px-Rome_(Italy)%2C_street_light_--_2013_--_3484.jpg"
	},
	[Detection.OBJECT_POLE]: {
		name: "Pole",
		osmTags: { power: "pole" },
		symbol: "https://upload.wikimedia.org/wikipedia/commons/thumb/6/61/Rusty_can_on_a_public_pole%2C_Winschoten_%282019%29_01.jpg/270px-Rusty_can_on_a_public_pole%2C_Winschoten_%282019%29_01.jpg"
	},
	[Detection.OBJECT_UTILITY_POLE]: {
		name: "Utility pole",
		osmTags: { man_made: "utility_pole" },
		symbol: "https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/Top_of_power_line_pole_-_east_side.jpg/320px-Top_of_power_line_pole_-_east_side.jpg"
	},
	[Detection.SIGN_RESERVED_PARKING]: {
		name: "Reserved parking",
		osmTags: { amenity: "parking_space", "capacity:disabled": 1 },
		symbol: "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Reserved_Parking_disabled_persons.jpg/320px-Reserved_Parking_disabled_persons.jpg"
	},
	[Detection.SIGN_ANIMAL_CROSSING]: {
		name: "Animal crossing sign",
		osmTags: { hazard: "animal_crossing" },
		symbol: "https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/Animal_crossing_sign.jpg/320px-Animal_crossing_sign.jpg"
	},
	[Detection.SIGN_RAILWAY_CROSSING]: {
		name: "Railway crossing sign",
		osmTags: { railway: "level_crossing" },
		symbol: "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/France_road_sign_A8.svg/320px-France_road_sign_A8.svg.png"
	}
};
