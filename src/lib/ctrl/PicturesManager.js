import CSVFetcher from "./fetchers/CSV";
import EventLauncher from "./EventLauncher";
import FlickrFetcher from "./fetchers/Flickr";
import LatLng from "../model/LatLng";
import LatLngBounds from "../model/LatLngBounds";
import Detection from "../model/Detection";
import MapillaryFetcher from "./fetchers/Mapillary";
import KartaViewFetcher from "./fetchers/KartaView";
import WikiCommonsFetcher from "./fetchers/WikiCommons";

const MAPILLARY_CLIENT_ID = "MLY|4186514921455326|31c8e0e5704a790a12cdb71c37dd4964";
const FLICKR_API_KEY = "1567bc982f9ad3c6a60ce8c736c62809";
const EARTH_RADIUS = 6371000;
const DEFAULT_CAMERA_ANGLE = 70;

const deg2rad = d => { return d * (Math.PI / 180); }
const rad2deg = r => { return r * (180 / Math.PI); }

/**
 * PicturesManager retrieves pictures metadata from all available pictures providers.
 * @example
 * ```js
 * let pm = new PicturesManager();
 * pm.startPicsRetrieval(new LatLngBounds(new LatLng(1.2, 48.7), new LatLng(1.3, 48.8)), { mindate: 754464600000 })
 * .then(pictures => {
 * 	for(let i=0; i < pictures.length; i++) {
 * 		console.log(pictures[i].pictureUrl);
 * 	}
 * });
 *
 * //Example with user-defined fetchers
 * let pmu = new PicturesManager({
 * 	userDefinedFetchers: {
 * 		ownfetch: {
 * 			csv: "http://my.server.net/own/index.csv", // Mandatory
 * 			name: "OwnFetcher", // Recommended
 * 			homepage: "http://my.server.net/own/", // Optional
 * 			logo: "http://my.server.net/own/logo.png", // Recommended
 * 			bbox: new LatLngBounds(new LatLng(1.2, 48.7), new LatLng(1.3, 48.8)), // Recommended if you server pictures in a specific country/area
 * 			license: "CC By-SA 3.0", // Optional, can be overriden in CSV index
 * 			user: "Default user name" // Optional, can be overriden in CSV index
 * 		}
 * 	}
 * });
 * ```
 * @param {object} [options] The options for the pictures manager.
 * @param {object} [options.fetcherCredentials] The credentials for pictures providers. As credentials are specific to each provider, you must see their fetcher documentation for details.
 * @param {object} [options.userDefinedFetchers] The CSV fetchers, as defined by user. Each one is identified by an unique ID.
 * @param {Array<String>} [options.usefetchers] The list of fetchers to use. Fetchers names must be all lowercase without spaces (example: mapillary, wikicommons, flickr). If `ignorefetchers` is also defined, only `usefetchers` parameter is considered. Can be overriden by individuals calls.
 * @param {Array<String>} [options.ignorefetchers] The list of fetchers to ignore. Fetchers names must be all lowercase without spaces (example: mapillary, wikicommons, flickr). Can be overriden by individuals calls.
 */
export default class PicturesManager extends EventLauncher {
	constructor(options) {
		super();

		//Define options
		options = options || {};
		let credentials = {
			mapillary: { key: MAPILLARY_CLIENT_ID },
			flickr: { key: FLICKR_API_KEY }
		};
		this.options = Object.assign({
			fetcherCredentials: credentials,
			userDefinedFetchers: null,
			useOnly: null,
			ignorefetchers: null,
			usefetchers: null
		}, options);

		//Overwrite credentials separately because Object.assign doesn't do fine merge for sub objects
		if(options.fetcherCredentials != undefined) {
			this.options.fetcherCredentials = Object.assign(credentials, options.fetcherCredentials);
		}

		//Fetchers definition
		this.fetchers = {
			mapillary: new MapillaryFetcher(this.options.fetcherCredentials.mapillary.key),
			flickr: new FlickrFetcher(this.options.fetcherCredentials.flickr.key),
			kartaview: new KartaViewFetcher(),
			wikicommons: new WikiCommonsFetcher()
		};

		//User defined fetchers
		if(this.options.userDefinedFetchers !== null) {
			for(const id in this.options.userDefinedFetchers) {
				const udf = this.options.userDefinedFetchers[id];

				if(this.fetchers[id] !== undefined) {
					throw new Error("Can't create user-defined fetcher "+id+": already exists");
				}

				if(udf.csv == null || udf.csv.length == 0) {
					throw new Error("Can't create user-defined fetcher "+id+": invalid csv parameter (must be an URL)");
				}

				if(udf.bbox !== undefined && !udf.bbox instanceof LatLngBounds) {
					throw new Error("Can't create user-defined fetcher "+id+": invalid bbox parameter (must be a LatLngBounds, or missing)");
				}

				this.fetchers[id] = new CSVFetcher(udf.csv, udf);
			}
		}

		return this;
	}

//ACCESSORS
	/**
	 * Get details about available fetchers.
	 * @return {Object} An object, where keys are fetcher IDs and values are an object { name, homepageUrl, logoUrl }.
	 */
	getFetcherDetails() {
		let details = {};

		for(let f in this.fetchers) {
			details[f] = {
				name: this.fetchers[f].name,
				homepageUrl: this.fetchers[f].homepageUrl,
				logoUrl: this.fetchers[f].logoUrl
			};
		}

		return details;
	}

//OTHER METHODS
	/**
	 * Ask for pictures metadata retrieval.
	 * You can listen to {@link #picturesmanagerfetcherdone|fetcherdone} and {@link #picturesmanagerfetcherfailed|fetcherfailed} events for watching progress.
	 * @param {LatLngBounds} boundingBox The area in which data must be retrieved.
	 * @param {object} [options] The options for pictures retrieval.
	 * @param {int} [options.mindate] The minimal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {int} [options.maxdate] The maximal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {Array<String>} [options.usefetchers] The list of fetchers to use. Fetchers names must be all lowercase without spaces (example: mapillary, wikicommons, flickr). If `ignorefetchers` is also defined, only `usefetchers` parameter is considered.
	 * @param {Array<String>} [options.ignorefetchers] The list of fetchers to ignore. Fetchers names must be all lowercase without spaces (example: mapillary, wikicommons, flickr).
	 * @return {Promise} A promise resolving on downloaded pictures
	 */
	startPicsRetrieval(boundingBox, options) {
		options = Object.assign({
			mindate: null,
			maxdate: null,
			usefetchers: null,
			ignorefetchers: null
		}, this.options, options);

		//Ignore "ignorefetchers" if "usefetchers" is defined
		if(options.usefetchers && options.ignorefetchers) {
			options.ignorefetchers = null;
		}

		const promises = [];

		for(const fId in this.fetchers) {
			if(
				(options.usefetchers == null || options.usefetchers.indexOf(fId) >= 0)
				&& (options.ignorefetchers == null || options.ignorefetchers.indexOf(fId) < 0)
			) {
				promises.push(
					this.fetchers[fId]
					.requestPictures(boundingBox, options)
					.then(p => {
						this.fire("fetcherdone", fId);
						return p;
					},
					e => {
						this.fire("fetcherfailed", fId);
						console.log(e);
					})
				);
			}
		}

		if(promises.length == 0) {
			throw new Error("ctrl.picturesmanager.picsretrieval.nofetchersused");
		}

		return Promise.all(promises)
		.then(values => {
			let prev = null;

			let result =
				values
				.filter(a => { return a !== null && a !== undefined; });

			if(result.length > 0) {
				result = result.reduce((a, b) => { return a.concat(b); })
				.sort((a, b) => { return b.date - a.date; })
				.filter(a => {
					if(a == undefined) {
						return false;
					}
					else if(!boundingBox.contains(a.coordinates)) {
						return false;
					}
					else {
						if(prev != null) {
							const looklike = a.lookAlike(prev);
							if(!looklike) {
								prev = a;
							}
							return !looklike;
						}
						else {
							prev = a;
							return true;
						}
					}
				});
			}

			return result;
		});
	}

	/**
	 * Ask for pictures metadata retrieval in a circle.
	 * You can listen to {@link #picturesmanagerfetcherdone|fetcherdone} and {@link #picturesmanagerfetcherfailed|fetcherfailed} events for watching progress.
	 * @param {LatLng} center The center of the circle.
	 * @param {int} radius The radius of the circle, in meters.
	 * @param {object} [options] The options for pictures retrieval.
	 * @param {int} [options.mindate] The minimal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {int} [options.maxdate] The maximal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {Array<String>} [options.usefetchers] The list of fetchers to use. Fetchers names must be all lowercase without spaces (example: mapillary, wikicommons, flickr). If `ignorefetchers` is also defined, only `usefetchers` parameter is considered.
	 * @param {Array<String>} [options.ignorefetchers] The list of fetchers to ignore. Fetchers names must be all lowercase without spaces (example: mapillary, wikicommons, flickr).
	 * @param {boolean} [options.towardscenter] Set to true if you only want pictures pointing towards center. Defaults to false.
	 * @param {int} [options.cameraAngle] The opening angle of camera in degrees (defaults to 70°)
	 * @return {Promise} A promise resolving on downloaded pictures
	 */
	startPicsRetrievalAround(center, radius, options) {
		let latRad = deg2rad(center.lat);
		let radiusOnLat = Math.cos(latRad) * EARTH_RADIUS;
		let deltaLat = rad2deg(radius / EARTH_RADIUS);
		let deltaLon = rad2deg(radius / radiusOnLat);

		let bbox = new LatLngBounds(new LatLng(center.lat - deltaLat, center.lng - deltaLon), new LatLng(center.lat + deltaLat, center.lng + deltaLon));
		const angle = options.cameraAngle && !isNaN(options.cameraAngle) ? parseInt(options.cameraAngle) : DEFAULT_CAMERA_ANGLE;

		if(!options.towardscenter) {
			return this.startPicsRetrieval(bbox, options);
		}
		else {
			return this.startPicsRetrieval(bbox, options).then(pictures => {
				return pictures.filter(p => {
					return p.details.isSpherical || (!isNaN(p.direction) && this._canBeSeen(p.coordinates, center, p.direction, angle));
				});
			});
		}
	}

	/**
	 * Ask for pictures availability summary in a given area.
	 * You can listen to {@link #picturesmanagerfetcherdone|fetcherdone} and {@link #picturesmanagerfetcherfailed|fetcherfailed} events for watching progress.
	 * @param {LatLngBounds} boundingBox The area in which data must be retrieved.
	 * @param {object} [options] The options for summary retrieval.
	 * @param {int} [options.mindate] The minimal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {int} [options.maxdate] The maximal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {Array<String>} [options.usefetchers] The list of fetchers to use. Fetchers names must be all lowercase without spaces (example: mapillary, wikicommons, flickr). If `ignorefetchers` is also defined, only `usefetchers` parameter is considered.
	 * @param {Array<String>} [options.ignorefetchers] The list of fetchers to ignore. Fetchers names must be all lowercase without spaces (example: mapillary, wikicommons, flickr).
	 * @return {Promise} A promise resolving on pictures statistics
	 */
	startSummaryRetrieval(boundingBox, options) {
		options = Object.assign({
			mindate: null,
			maxdate: null,
			usefetchers: null,
			ignorefetchers: null
		}, this.options, options);

		//Ignore "ignorefetchers" if "usefetchers" is defined
		if(options.usefetchers && options.ignorefetchers) {
			options.ignorefetchers = null;
		}

		const promises = [];

		for(const fId in this.fetchers) {
			if(
				(options.usefetchers == null || options.usefetchers.indexOf(fId) >= 0)
				&& (options.ignorefetchers == null || options.ignorefetchers.indexOf(fId) < 0)
			) {
				promises.push(
					this.fetchers[fId]
					.requestSummary(boundingBox, options)
					.then(s => {
						this.fire("fetcherdone", fId);
						return s;
					},
					e => {
						this.fire("fetcherfailed", fId);
						console.log(e);
					})
				);
			}
		}

		return Promise.all(promises)
		.then(values => {
			values = values.filter(v => v != null);
			const result = { last: 0, amount: 0, approxAmount: false };

			for(const s in values) {
				if(values[s].last > result.last) { result.last = values[s].last; }
				result.approxAmount = result.approxAmount || values[s].amount.charAt(0) == ">";
				result.amount += parseInt(values[s].amount.substring(1));
			}

			return result;
		});
	}

	/**
	 * Ask for features images providers have automatically detected.
	 * You can listen to {@link #picturesmanagerfetcherdone|fetcherdone} and {@link #picturesmanagerfetcherfailed|fetcherfailed} events for watching progress.
	 * @param {LatLngBounds} boundingBox The area in which data must be retrieved.
	 * @param {object} [options] The options for detections retrieval.
	 * @param {int[]} [options.types] The list of feature types to look for (use Detection.* constants)
	 * @param {Array<String>} [options.usefetchers] The list of fetchers to use. Fetchers names must be all lowercase without spaces (example: mapillary, wikicommons, flickr). If `ignorefetchers` is also defined, only `usefetchers` parameter is considered.
	 * @param {Array<String>} [options.ignorefetchers] The list of fetchers to ignore. Fetchers names must be all lowercase without spaces (example: mapillary, wikicommons, flickr).
	 * @param {boolean} [options.asgeojson] Set to true if result should be returned as a GeoJSON feature collection
	 * @return {Promise} A promise resolving on downloaded detections
	 */
	startDetectionsRetrieval(boundingBox, options) {
		options = Object.assign({
			types: [],
			usefetchers: null,
			ignorefetchers: null
		}, this.options, options);

		//Ignore "ignorefetchers" if "usefetchers" is defined
		if(options.usefetchers && options.ignorefetchers) {
			options.ignorefetchers = null;
		}

		// Defaults to Mapillary only
		if(options.usefetchers === null && options.ignorefetchers === null) {
			options.usefetchers = ["mapillary"];
		}

		const promises = [];

		for(const fId in this.fetchers) {
			if(
				(options.usefetchers == null || options.usefetchers.indexOf(fId) >= 0)
				&& (options.ignorefetchers == null || options.ignorefetchers.indexOf(fId) < 0)
			) {
				promises.push(
					this.fetchers[fId]
					.requestDetections(boundingBox, options)
					.then(p => {
						this.fire("fetcherdone", fId);
						return p;
					},
					e => {
						this.fire("fetcherfailed", fId);
						console.log(e);
					})
				);
			}
		}

		if(promises.length == 0) {
			throw new Error("ctrl.picturesmanager.detectionsretrieval.nofetchersused");
		}

		return Promise.all(promises)
		.then(values => {
			let result =
				values
				.filter(a => { return a !== null && a !== undefined; });

			if(result.length > 0) {
				result = result.reduce((a, b) => { return a.concat(b); })
				.sort((a, b) => { return b.date - a.date; })
				.filter(a => {
					if(a == undefined) {
						return false;
					}
					else if(!boundingBox.contains(a.coordinates)) {
						return false;
					}
					else {
						return true;
					}
				});
			}

			if(options.asgeojson) {
				return { type: "FeatureCollection", features: result.map(f => {
					return {
						type: "Feature",
						geometry: { type: "Point", coordinates: [ f.coordinates.lng, f.coordinates.lat ] },
						properties: Object.assign(
							{},
							Detection.TYPE_DETAILS[f.type].osmTags,
							{ "source:geometry": (this.fetchers[f.provider].name || f.provider) + " " + (new Date(f.date)).toISOString().split('T')[0] }
						)
					};
				}) };
			}
			else {
				return result;
			}
		});
	}

	/**
	 * Read pictures URL from OpenStreetMap tags.
	 * @param {Object} tags The OpenStreetMap tags.
	 * @return {string[]} A list of read pictures URL.
	 */
	getPicturesFromTags(tags) {
		return [].concat(
			...Object.keys(this.fetchers)
			.filter(k => k!="flickr")
			.map(k => this.fetchers[k].tagsToPictures(tags))
		);
	}

	/**
	 * Can an end point be seen from a start point, according to direction and opening of camera on start point ?
	 * @param {LatLng} start The start point (camera)
	 * @param {LatLng} end The end point (object)
	 * @param {int} startDirection The direction camera points to (0-360 North based)
	 * @param {int} startOpening Camera opening angle (degrees)
	 * @return {boolean} True if camera sees end point
	 * @private
	 */
	_canBeSeen(start, end, startDirection, startOpening) {
		let minDir = startDirection - startOpening / 2;
		if(minDir < 0) { minDir += 360; }
		let maxDir = startDirection + startOpening / 2;
		if(maxDir < 0) { maxDir += 360; }
		//console.log(minDir, startDirection, maxDir);

		const dist = Math.sqrt(Math.pow(end.lat-start.lat, 2) + Math.pow(end.lng-start.lng, 2));
		//console.log(dist);

		const minCoord = new LatLng(dist * Math.cos(deg2rad(minDir)) + start.lat, dist * Math.sin(deg2rad(minDir)) + start.lng);
		const maxCoord = new LatLng(dist * Math.cos(deg2rad(maxDir)) + start.lat, dist * Math.sin(deg2rad(maxDir)) + start.lng);
		//console.log(minCoord, maxCoord);

		const bounds = new LatLngBounds(minCoord, maxCoord);

		if(start.lat === end.lat) {
			//console.log("same lat", bounds.getSouth() < end.lat, bounds.getNorth() > end.lat);
			return Math.abs(end.lng - minCoord.lng) <= dist && bounds.getSouth() < end.lat && bounds.getNorth() > end.lat;
		}
		else if(start.lng === end.lng) {
			//console.log("same lng", bounds.getWest() < end.lng, bounds.getEast() > end.lng);
			return Math.abs(end.lat - minCoord.lat) <= dist && bounds.getWest() < end.lng && bounds.getEast() > end.lng;
		}
		else {
			return bounds.contains(end);
		}
	}

	/**
	 * Event sent when a fetcher has done its data retrieval.
	 * @event PicturesManager#fetcherdone
	 * @type {string} The fetcher identifier.
	 * @example
	 * let pm = new PicturesManager();
	 * pm.on("fetcherdone", fetcherId => {
	 * 	console.log("Fetcher "+fetcherId+" has done its job");
	 * });
	 * pm.startPicsRetrieval(new LatLngBounds(new LatLng(1.2, 42.3), new LatLng(1.3, 42.4)));
	 */

	/**
	 * Event sent when a fetcher has failed to retrieve data.
	 * @event PicturesManager#fetcherfailed
	 * @type {string} The fetcher identifier.
	 * @example
	 * let pm = new PicturesManager();
	 * pm.on("fetcherfailed", fetcherId => {
	 * 	console.log("Fetcher "+fetcherId+" has failed");
	 * });
	 * pm.startPicsRetrieval(new LatLngBounds(new LatLng(1.2, 42.3), new LatLng(1.3, 42.4)));
	 */
}
