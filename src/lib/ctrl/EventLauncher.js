/**
 * EventLauncher is a class able to launch events on its name. You can listen to events using {@link #EventLauncher#on} method, and trigger event using {@link #EventLauncher#fire} method.
 * @private
 */
export default class EventLauncher {
	constructor() {
		if(this.constructor.name === "EventLauncher") {
			throw new TypeError("ctrl.eventlauncher.cantinstantiate");
		}
		
		this.eventListeners = {};
		
		return this;
	}

//OTHER METHODS
	/**
	 * Add a new listener on a given event.
	 * @param {string} event The event to listen to.
	 * @param {function} handler The handling function, which will be called when event is fired.
	 * @return {EventLauncher} this
	 */
	on(event, handler) {
		if(this.eventListeners[event] === undefined) {
			this.eventListeners[event] = [];
		}
		this.eventListeners[event].push(handler);
		
		return this;
	}
	
	/**
	 * Removes a listener of an event.
	 * @param {string} event The concerned event.
	 * @param {function} [handler] The handler to remove. If no handler is given, all handlers for this events are removed.
	 * @return {EventLauncher} this
	 */
	off(event, handler) {
		if(handler === undefined || handler === null) {
			this.eventListeners[event] = undefined;
		}
		else if(this.eventListeners[event] !== undefined) {
			while(this.eventListeners[event].indexOf(handler) >= 0) {
				this.eventListeners[event].splice(this.eventListeners[event].indexOf(handler), 1);
			}
		}
		
		return this;
	}
	
	/**
	 * Fires an event. It calls all listeners associated to the given event, passing the given data as argument.
	 * @param {string} event The event to fire.
	 * @param {object} [data] The associated data.
	 * @return {EventLauncher} this
	 */
	fire(event, data) {
		if(this.eventListeners[event] !== undefined) {
			for(const f of this.eventListeners[event]) {
				setTimeout(() => {
					f(data);
				}, 0);
			}
		}
		
		return this;
	}
}
