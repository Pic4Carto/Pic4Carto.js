import Fetcher from "../Fetcher";
import LatLng from "../../model/LatLng";
import LatLngBounds from "../../model/LatLngBounds";
import Picture from "../../model/Picture";
import Detection from "../../model/Detection";

// Transform standard detection types into Mapillary types
const DTC_TO_MPL = {
	[Detection.OBJECT_BENCH]: "object--bench",
	[Detection.SIGN_STOP]: [
		"regulatory--stop--g1",
		"regulatory--stop--g2",
		"regulatory--stop--g3",
		"regulatory--stop--g4",
		"regulatory--stop--g5",
		"regulatory--stop--g6",
		"regulatory--stop--g7",
		"regulatory--stop--g8",
		"regulatory--stop--g9",
		"regulatory--stop--g10"
	],
	[Detection.MARK_CROSSING]: [ "marking--discrete--crosswalk-zebra", "construction--flat--crosswalk-plain" ],
	[Detection.OBJECT_BICYCLE_PARKING]: "object--bike-rack",
	[Detection.OBJECT_CCTV]: [ "object--cctv-camera", "information--camera--g1", "information--camera--g2", "information--camera--g3" ],
	[Detection.OBJECT_HYDRANT]: "object--fire-hydrant",
	[Detection.OBJECT_POSTBOX]: "object--mailbox",
	[Detection.OBJECT_MANHOLE]: "object--manhole",
	[Detection.OBJECT_PARKING_METER]: "object--parking-meter",
	[Detection.OBJECT_PHONE]: [ "object--phone-booth", "information--telephone--g1", "information--telephone--g2" ],
	[Detection.SIGN_ADVERT]: "object--sign--advertisement",
	[Detection.SIGN_INFO]: "object--sign--information",
	[Detection.SIGN_STORE]: "object--sign--store",
	[Detection.OBJECT_STREET_LIGHT]: "object--street-light",
	[Detection.OBJECT_POLE]: "object--support--pole",
	[Detection.OBJECT_UTILITY_POLE]: "object--support--utility-pole",
	[Detection.SIGN_RESERVED_PARKING]: [
		"regulatory--reserved-parking--g1",
		"information--disabled-persons--g1",
		"information--disabled-persons--g2",
		"information--disabled-persons--g3",
		"complementary--disabled-persons--g1"
	],
	[Detection.SIGN_ANIMAL_CROSSING]: [
		"warning--camel-crossing--g1",
		"warning--camel-crossing--g2",
		"warning--domestic-animals--g1",
		"warning--domestic-animals--g2",
		"warning--domestic-animals--g3",
		"warning--domestic-animals--g4",
		"warning--domestic-animals--g5",
		"warning--domestic-animals--g6",
		"warning--domestic-animals--g7",
		"warning--domestic-animals--g8",
		"warning--elephant-crossing--g1",
		"warning--frog-crossing--g1",
		"warning--horse-crossing--g1",
		"warning--kangaroo-crossing--g1",
		"warning--kiwi-crossing--g1",
		"warning--kiwi-crossing--g2",
		"warning--koala-crossing--g1",
		"warning--koala-crossing--g2",
		"warning--koala-crossing--g3",
		"warning--koala-crossing--g4",
		"warning--monkey-crossing--g1",
		"warning--panda-crossing--g1",
		"warning--rabbit-crossing--g1",
		"warning--raccoon-crossing--g1",
		"warning--wild-animals--g1",
		"warning--wild-animals--g2",
		"warning--wild-animals--g3",
		"warning--wild-animals--g4",
		"warning--wild-animals--g5",
		"warning--wild-animals--g6",
		"warning--wild-animals--g7",
		"warning--wild-animals--g8",
		"warning--wombat-crossing--g1"
	],
	[Detection.SIGN_RAILWAY_CROSSING]: [
		"warning--railroad-crossing-with-barriers--g1",
		"warning--railroad-crossing-with-barriers--g2",
		"warning--railroad-crossing-with-barriers--g3",
		"warning--railroad-crossing-with-barriers--g4",
		"warning--railroad-crossing-with-barriers--g5",
		"warning--railroad-crossing-with-barriers--g6",
		"warning--railroad-crossing-with-barriers--g7",
		"warning--railroad-crossing-without-barriers--g1",
		"warning--railroad-crossing-without-barriers--g2",
		"warning--railroad-crossing-without-barriers--g3",
		"warning--railroad-crossing-without-barriers--g4",
		"warning--railroad-crossing-without-barriers--g5",
		"warning--railroad-crossing-without-barriers--g6",
		"warning--railroad-intersection--g1",
		"warning--railroad-intersection--g2",
		"warning--railroad-intersection--g3",
		"warning--railroad-intersection--g4",
		"warning--railroad-intersection--g5",
		"warning--railroad-intersection--g6",
		"warning--railroad-intersection--g7",
		"warning--railroad-intersection--g8",
		"warning--railroad-intersection--g9"
	]
};

// Transform mapillary types into standard types
const MPL_TO_DTC = {};
Object.entries(DTC_TO_MPL).forEach(e => {
	if(typeof e[1] === "string") {
		MPL_TO_DTC[e[1]] = parseInt(e[0]);
	}
	else {
		e[1].forEach(v => { MPL_TO_DTC[v] = parseInt(e[0]); });
	}
});

/**
 * Mapillary fetcher.
 * @example
 * <caption>Credentials for Mapillary Fetcher (see PicturesManager)</caption>
 * fetcherCredentials: {
 * 	mapillary: { key: "APIKEY" }
 * }
 */
export default class Mapillary extends Fetcher {
//CONSTRUCTOR
	/**
	 * Class constructor.
	 * @param clientId The client ID for the Mapillary API
	 */
	constructor(clientId) {
		super();
		if(typeof clientId === "string" && clientId.length > 0) {
			this.clientId = clientId;
		}
		else {
			throw new Error("ctrl.fetchers.mapillary.invalidclientid");
		}
	}

//ACCESSORS
	/**
	 * Get this provider name.
	 * @return {string} The provider human-readable name.
	 */
	get name() {
		return "Mapillary";
	}

	/**
	 * Get this provider logo URL.
	 * @return {string} The provider logo URL.
	 */
	get logoUrl() {
		return "https://upload.wikimedia.org/wikipedia/commons/5/51/Mapillary_dotcompass_2019.png";
	}

	/**
	 * Get this provider homepage URL.
	 * @return {string} The provider homepage URL.
	 */
	get homepageUrl() {
		return "https://www.mapillary.com/";
	}

//OTHER METHODS
	/**
	 * Get all the images in the given area for this provider.
	 * To get pictures when ready, suscribe to the {@link #fetcherdonepictures|donepictures} event.
	 * @param {LatLngBounds} boundingBox The bounding box of the area where you want to get pictures.
	 * @param {Object} [options] The options for pictures retrieval.
	 * @param {int} [options.mindate] The minimal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {int} [options.maxdate] The maximal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @return {Promise} A promise resolving on pictures as an array.
	 */
	requestPictures(boundingBox, options = {}) {
		// Get first part of metadata: images ID, timestamp, geometry
		const url = "https://graph.mapillary.com/images?fields=id,camera_type,captured_at,computed_compass_angle,computed_geometry&bbox="
			+ [boundingBox.getWest(), boundingBox.getSouth(), boundingBox.getEast(), boundingBox.getNorth()].join(",")
			+ "&access_token=" + this.clientId;

		return this.ajax(url, "json")
		.then(res => {
			if(!res || !res.data) { return Promise.reject(new Error("ctrl.fetcher.mapillary.getpicturesfailed")); }

			// Filter by timestamp if any option set
			const images = {};
			res.data
			.filter(pic => (
				!options.mindate || (new Date(pic.captured_at)).getTime() >= options.mindate)
				&& (!options.maxdate || (new Date(pic.captured_at)).getTime() <= options.maxdate)
			)
			.forEach(pic => {
				images[pic.id] = pic;
			});

			if(Object.keys(images).length === 0) { return []; }

			const url2 = "https://graph.mapillary.com/images?image_ids="
				+ Object.keys(images).join(",")
				+ "&fields=fields=id,thumb_256_url,thumb_2048_url"
				+ "&access_token=" + this.clientId;

			return this.ajax(url2, "json")
			.then(res2 => {
				if(!res2 || !res2.data) { return Promise.reject(new Error("ctrl.fetcher.mapillary.getpicturesfailed")); }

				res2.data.forEach(pic => {
					images[pic.id].url = pic.thumb_2048_url;
					images[pic.id].thumb = pic.thumb_256_url;
				});

				return Object.values(images)
				.filter(pic => pic.url && pic.captured_at && pic.computed_geometry)
				.map(pic => (new Picture(
					pic.url,
					(new Date(pic.captured_at)).getTime(),
					new LatLng(pic.computed_geometry.coordinates[1], pic.computed_geometry.coordinates[0]),
					this.name,
					"Mapillary contributor",
					"CC By-SA 4.0",
					"https://www.mapillary.com/app/?pKey=" + pic.id + "&lat=" + pic.computed_geometry.coordinates[1] + "&lng=" + pic.computed_geometry.coordinates[0] + "&focus=photo",
					pic.computed_compass_angle,
					{ mapillary: pic.id.toString() },
					pic.thumb,
					{ isSpherical: pic.camera_type === "spherical" }
				)));
			});
		});
	}

	/**
	 * Get summary statistics about pictures available in the given area.
	 * To get summary when ready, suscribe to the {@link #fetcherdonesummary|donesummary} event.
	 * @param {LatLngBounds} boundingBox The bounding box of the area where you want to get the summary.
	 * @param {Object} [options] The options for summary retrieval.
	 * @param {int} [options.mindate] The minimal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {int} [options.maxdate] The maximal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @return {Promise} A promise resolving on pictures summary
	 */
	requestSummary(boundingBox, options = {}) {
		return this.requestPictures(boundingBox, options)
		.then(pics => {
			if(pics.length > 0) {
				//Search most recent picture date
				pics.sort((a,b) => { return b.date - a.date; });

				return {
					last: pics[0].date,
					amount: "e"+pics.length,
					bbox: boundingBox.toBBoxString()
				};
			}
			else {
				return {
					amount: "e0",
					bbox: boundingBox.toBBoxString()
				};
			}
		})
		.catch(e => {
			console.error(e);
			this.fail(null, null, new Error("ctrl.fetcher.mapillary.getsummaryfailed"));
		});
	}

	/**
	 * Get feature detections in the given area.
	 * To get detections when ready, suscribe to the {@link #fetcherdonedetections|donedetections} event.
	 * @param {LatLngBounds} boundingBox The bounding box of the area where you want to get the detections.
	 * @param {object} [options] The options for detections retrieval.
	 * @param {int[]} [options.types] The list of detections types to look for (use Detection.* constants).
	 * @return {Promise} A promise resolving on detections
	 */
	requestDetections(boundingBox, options = {}) {
		const url = "https://graph.mapillary.com/map_features?fields=id,object_value,last_seen_at,geometry&bbox="
			+ [boundingBox.getWest(), boundingBox.getSouth(), boundingBox.getEast(), boundingBox.getNorth()].join(",")
			+ (options.types ? "&object_values=" + options.types.map(ot => typeof DTC_TO_MPL[ot] === "string" ? DTC_TO_MPL[ot] : DTC_TO_MPL[ot].join(",")).join(",") : "")
			+ "&access_token=" + this.clientId;

		return this.ajax(url, "json")
		.then(res => {
			if(!res || !res.data) { return Promise.reject(new Error("ctrl.fetcher.mapillary.getdetectionsfailed")); }

			return res.data.map(dtc => new Detection(
				MPL_TO_DTC[dtc.object_value],
				new LatLng(dtc.geometry.coordinates[1], dtc.geometry.coordinates[0]),
				(new Date(dtc.last_seen_at)).getTime(),
				"mapillary"
			));
		});
	}

	/**
	 * Get a list of pictures URL according to given OSM tags.
	 * @param {Object} tags The OSM tags to interpret.
	 * @return {string[]} The list of pictures URL read from tags.
	 */
	tagsToPictures(tags) {
		const result = [];

		Object.keys(tags).filter(k => k.startsWith("mapillary")).forEach(k => {
			tags[k].split(";").forEach(mid => {
				if(/^[0-9A-Za-z_\-]{22}$/.test(mid.trim())) {
					result.push("https://images.mapillary.com/"+mid.trim()+"/thumb-2048.jpg");
				}
			});
		});

		return result;
	}
}
