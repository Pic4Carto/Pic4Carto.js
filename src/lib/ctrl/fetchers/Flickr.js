import Fetcher from "../Fetcher";
import LatLng from "../../model/LatLng";
import LatLngBounds from "../../model/LatLngBounds";
import Picture from "../../model/Picture";

/**
 * Flickr fetcher.
 * @example
 * <caption>Credentials for Flickr Fetcher (see PicturesManager)</caption>
 * fetcherCredentials: {
 * 	flickr: { key: "APIKEY" }
 * }
 */
export default class Flickr extends Fetcher {
//CONSTRUCTOR
	/**
	 * Class constructor
	 * @param apiKey The API key for Flickr
	 */
	constructor(apiKey) {
		super();
		if(typeof apiKey === "string" && apiKey.length > 0) {
			this.apiKey = apiKey;
		}
		else {
			throw new Error("ctrl.fetchers.flickr.invalidapikey");
		}
	}

//ACCESSORS
	/**
	 * Get this provider name.
	 * @return {string} The provider human-readable name.
	 */
	get name() {
		return "Flickr";
	}
	
	/**
	 * Get this provider logo URL.
	 * @return {string} The provider logo URL.
	 */
	get logoUrl() {
		return "https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/Flickr_wordmark.svg/640px-Flickr_wordmark.svg.png";
	}
	
	/**
	 * Get this provider homepage URL.
	 * @return {string} The provider homepage URL.
	 */
	get homepageUrl() {
		return "https://www.flickr.com/";
	}

	/**
	 * Get all the images in the given area for this provider.
	 * To get pictures when ready, suscribe to the {@link #fetcherdonepictures|donepictures} event.
	 * @param {LatLngBounds} boundingBox The bounding box of the area where you want to get pictures.
	 * @param {object} [options] The options for pictures retrieval.
	 * @param {int} [options.mindate] The minimal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {int} [options.maxdate] The maximal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @return {Promise} A promise resolving on pictures as an array.
	 */
	requestPictures(boundingBox, options) {
		let data = Object.assign({
			picsPerRequest: 100,
			page: 1,
			pictures: [],
			bbox: new LatLngBounds(boundingBox.getSouthWest().wrap(), boundingBox.getNorthEast().wrap())
		}, this.options, options);
		
		if(!this.licenses) {
			return this.downloadLicenses(data);
		}
		else {
			return this.download(data);
		}
	}
	
	/**
	 * Get summary statistics about pictures available in the given area.
	 * To get summary when ready, suscribe to the {@link #fetcherdonesummary|donesummary} event.
	 * @param {LatLngBounds} boundingBox The bounding box of the area where you want to get the summary.
	 * @param {object} [options] The options for summary retrieval.
	 * @param {int} [options.mindate] The minimal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {int} [options.maxdate] The maximal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @return {Promise} A promise resolving on pictures summary
	 */
	requestSummary(boundingBox, options) {
		options = Object.assign({}, this.options, options);
		
		let url = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key="
					+ this.apiKey
					+ ((options.mindate != null) ? "&min_taken_date=" + (new Date(options.mindate)).toISOString().split('T')[0] : "")
					+ ((options.maxdate != null) ? "&max_taken_date=" + (new Date(options.maxdate)).toISOString().split('T')[0] : "")
					+ "&bbox=" + boundingBox.getWest() + "%2C" + boundingBox.getSouth() + "%2C" + boundingBox.getEast() + "%2C" + boundingBox.getNorth()
					+ "&has_geo=1"
					+ "&per_page=1"
					+ "&license=4,5,7,8,9,10"
					+ "&format=json&nojsoncallback=1&extras=date_taken";
		
		return this.ajax(url, "json")
			.then(data => {
				if(data === null || data.photos === undefined || data.photos.total === undefined || data.photos.photo === undefined) {
					this.fail(null, null, new Error("ctrl.fetcher.flickr.getsummaryfailed"));
				}
				
				//Parse data
				if(parseInt(data.photos.total) > 0) {
					return {
						last: (new Date(data.photos.photo[0].datetaken.replace(" ", "T"))).getTime(),
						amount: "e"+data.photos.total,
						bbox: boundingBox.toBBoxString()
					};
				}
				else {
					return {
						amount: "e0",
						bbox: boundingBox.toBBoxString()
					};
				}
			});
	}

//OTHER METHODS
	/**
	 * Retrieves pictures metadata for current bounding box, page and date.
	 * @private
	 */
	downloadLicenses(info) {
		return this.ajax(
				"https://api.flickr.com/services/rest/?method=flickr.photos.licenses.getInfo&api_key="+this.apiKey+"&format=json&nojsoncallback=1",
				"json"
			).then(d => {
				this.licenses = {};
				if(d.stat && d.stat == "ok" && d.licenses && d.licenses.license) {
					for(let i=0; i < d.licenses.license.length; i++) {
						let lcs = d.licenses.license[i];
						this.licenses[lcs.id] = lcs.name;
					}
					
					return this.download(info);
				}
				else {
					throw new Error("ctrl.fetcher.flickr.invalidlicensedata");
				}
			});
	}
	
	/**
	 * Retrieves pictures metadata for current bounding box, page and date.
	 * @private
	 */
	download(info) {
		let licenseList = Object.keys(this.licenses).filter((v) => { return v >= 4 && v != 6; });
		
		let url = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key="
			+ this.apiKey
			+ ((info.mindate != null) ? "&min_taken_date=" + (new Date(info.mindate)).toISOString().split('T')[0] : "")
			+ ((info.maxdate != null) ? "&max_taken_date=" + (new Date(info.maxdate)).toISOString().split('T')[0] : "")
			+ "&bbox=" + info.bbox.getWest() + "%2C" + info.bbox.getSouth() + "%2C" + info.bbox.getEast() + "%2C" + info.bbox.getNorth()
			+ "&has_geo=1"
			+ "&per_page=" + info.picsPerRequest
			+ "&page=" + info.page
			+ "&license=" + licenseList.join(",")
			+ "&format=json&nojsoncallback=1&extras=license%2Cdate_taken%2Cowner_name%2Cgeo%2Curl_l%2Curl_z";
		
		return this.ajax(url, "json")
			.then(result => {
				if(result === null || result.photos === undefined || result.photos.photo === undefined) {
					throw new Error("ctrl.fetcher.flickr.getpicturesfailed");
				}
				
				//Parse data
				if(result.stat && result.stat === "ok" && result.photos.photo && result.photos.photo.length > 0) {
					for(let i=0; i < result.photos.photo.length; i++) {
						let pic = result.photos.photo[i];
						
						if(pic.url_l && pic.datetaken && pic.latitude && pic.longitude && pic.license > 0) {
							info.pictures.push(new Picture(
								pic.url_l,
								(new Date(pic.datetaken.replace(" ", "T"))).getTime(),
								new LatLng(pic.latitude, pic.longitude),
								this.name,
								pic.ownername,
								this.licenses[pic.license],
								"https://www.flickr.com/photos/"+pic.owner+"/"+pic.id,
								null,
								{ flickr: "https://www.flickr.com/photos/"+pic.owner+"/"+pic.id },
								pic.url_z
							));
						}
					}
					
					info.page = (result.photos.photo.length == info.picsPerRequest) ? info.page + 1 : -1;
				}
				else {
					info.page = -1;
				}
				
				//Next step
				if(info.page > 0) {
					return this.download(info);
				}
				else {
					return info.pictures;
				}
			});
	}
}
