import Fetcher from "../Fetcher";
import LatLng from "../../model/LatLng";
import LatLngBounds from "../../model/LatLngBounds";
import Picture from "../../model/Picture";
import {parse as CSVParser} from "csv-parse/browser/esm/sync";
import KDBush from "kdbush";

/**
 * CSV fetcher. Allows to serve statically stored images, indexed by a CSV file.
 * It doesn't need any credentials.
 */
export default class CSV extends Fetcher {
//CONSTRUCTOR
	/**
	 * Class constructor.
	 * @param {string} csv The CSV URL
	 * @param {object} [options] The fetcher options
	 * @param {string} [options.name] The fetcher human-readable name
	 * @param {string} [options.homepage] The fetcher homepage URL
	 * @param {string} [options.logo] The fetcher logo URL
	 * @param {LatLngBounds} [options.bbox] The bounding box where data is available (to avoid useless calls)
	 * @param {string} [options.license] The default license of served pictures
	 * @param {string} [options.user] The default user name of served pictures
	 */
	constructor(csv, options) {
		super();
		
		if(typeof csv === "string" && csv.length > 0) {
			this.csvURL = csv;
		}
		else {
			throw new Error("ctrl.fetchers.csv.invalidcsvurl");
		}
		
		this.options = Object.assign({
			bbox: null,
			license: "Unknown license",
			user: "Unknown user"
		}, options);
		
		this.csv = null;
		this.tree = null;
		this.isDownloading = false;
	}

//ACCESSORS
	/**
	 * Get this provider name.
	 * @return {string} The provider human-readable name.
	 */
	get name() {
		return (this.options && this.options.name) || "CSV Source";
	}
	
	/**
	 * Get this provider logo URL.
	 * @return {string} The provider logo URL.
	 */
	get logoUrl() {
		return (this.options && this.options.logo) || "";
	}
	
	/**
	 * Get this provider homepage URL.
	 * @return {string} The provider homepage URL.
	 */
	get homepageUrl() {
		return (this.options && this.options.homepage) || "";
	}

//OTHER METHODS
	/**
	 * Get all the images in the given area for this provider.
	 * To get pictures when ready, suscribe to the {@link #fetcherdonepictures|donepictures} event.
	 * @param {LatLngBounds} boundingBox The bounding box of the area where you want to get pictures.
	 * @param {Object} [options] The options for pictures retrieval.
	 * @param {int} [options.mindate] The minimal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {int} [options.maxdate] The maximal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @return {Promise} A promise resolving on pictures as an array.
	 */
	requestPictures(boundingBox, options) {
		return new Promise((resolve, reject) => {
			options = options || {};
			const bbox = new LatLngBounds(boundingBox.getSouthWest().wrap(), boundingBox.getNorthEast().wrap());
			
			if(this.options.bbox === null || this.options.bbox.intersects(bbox)) {
				this.getCSV()
					.then(() => {
						//Read pictures index
						const result = this.tree
							.range(bbox.getWest(), bbox.getSouth(), bbox.getEast(), bbox.getNorth())
							.map(id => this.csv[id])
							.filter(p => (options.mindate == null || options.mindate <= p.timestamp * 1000) && (options.maxdate == null || options.maxdate >= p.timestamp * 1000))
							.map(p => new Picture(
								p.picture_url,
								p.timestamp*1000,
								new LatLng(p.latitude, p.longitude),
								this.name,
								p.user || this.options.user,
								p.license || this.options.license,
								p.details_url || p.picture_url,
								!isNaN(p.direction) ? parseInt(p.direction) : null,
								{ image: p.picture_url }
							));
						
						//Send event for selected pictures
						resolve(result);
					})
					.catch(reject);
			}
			else {
				//Out of bounds, so no pictures returned
				resolve([]);
			}
		});
	}
	
	/**
	 * Get summary statistics about pictures available in the given area.
	 * To get summary when ready, suscribe to the {@link #fetcherdonesummary|donesummary} event.
	 * @param {LatLngBounds} boundingBox The bounding box of the area where you want to get the summary.
	 * @param {Object} [options] The options for summary retrieval.
	 * @param {int} [options.mindate] The minimal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {int} [options.maxdate] The maximal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @return {Promise} A promise resolving on pictures summary
	 */
	requestSummary(boundingBox, options) {
		return new Promise((resolve, reject) => {
			options = options || {};
			const bbox = new LatLngBounds(boundingBox.getSouthWest().wrap(), boundingBox.getNorthEast().wrap());
			
			if(this.options.bbox === null || this.options.bbox.intersects(bbox)) {
				this.getCSV()
					.then(() => {
						let last = null;
						let count = 0;
						
						//Read pictures index
						this.tree.range(bbox.getWest(), bbox.getSouth(), bbox.getEast(), bbox.getNorth())
							.map(id => this.csv[id])
							.filter(p => (options.mindate == null || options.mindate <= p.timestamp * 1000) && (options.maxdate == null || options.maxdate >= p.timestamp * 1000))
							.forEach(p => {
								count++;
								if(last === null || last < p.timestamp * 1000) { last = p.timestamp * 1000; }
							});
						
						//Send event for selected pictures
						resolve({
							last: last,
							amount: "e"+count,
							bbox: bbox.toBBoxString()
						});
					})
					.catch(reject);
			}
			else {
				//Out of bounds, so no pictures returned
				resolve({
					amount: "e0",
					bbox: bbox.toBBoxString()
				});
			}
		});
	}

	/**
	 * Helper method for populating csv property.
	 * If not populated, CSV file is downloaded and parsed. If already available, then it is returned directly.
	 * @return {Promise} A promise resolving when CSV data is ready.
	 * @private
	 */
	getCSV() {
		return new Promise((resolve, reject) => {
			if(this.tree !== null) {
				resolve();
			}
			else if(!this.isDownloading) {
				this.isDownloading = true;
				this.ajax(this.csvURL, "csv")
					.then(d => {
						this.csv = CSVParser(d, { columns: true, delimiter: ';' });
						this.tree = new KDBush(this.csv.length);
						this.csv.forEach(p => {
							this.tree.add(p.longitude, p.latitude);
						});
						this.tree.finish();
						resolve();
					})
					.catch(reject);
			}
			else {
				setTimeout(() => {
					this.getCSV().then(resolve);
				}, 100);
			}
		});
	}
}
