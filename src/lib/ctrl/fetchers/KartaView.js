import Fetcher from "../Fetcher";
import LatLng from "../../model/LatLng";
import LatLngBounds from "../../model/LatLngBounds";
import Picture from "../../model/Picture";

const API_URL = "https://api.openstreetcam.org/1.0/";

/**
 * KartaView fetcher.
 */
export default class KartaView extends Fetcher {
//ACCESSORS
	/**
	 * Get this provider name.
	 * @return {string} The provider human-readable name.
	 */
	get name() {
		return "KartaView";
	}
	
	/**
	 * Get this provider logo URL.
	 * @return {string} The provider logo URL.
	 */
	get logoUrl() {
		return "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0e/KartaView_wordmark.svg/640px-KartaView_wordmark.svg.png";
	}
	
	/**
	 * Get this provider homepage URL.
	 * @return {string} The provider homepage URL.
	 */
	get homepageUrl() {
		return "https://kartaview.org/";
	}

//OTHER METHODS
	/**
	 * Get all the images in the given area for this provider.
	 * To get pictures when ready, suscribe to the {@link #fetcherdonepictures|donepictures} event.
	 * @param {LatLngBounds} boundingBox The bounding box of the area where you want to get pictures.
	 * @param {object} [options] The options for pictures retrieval.
	 * @param {int} [options.mindate] The minimal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {int} [options.maxdate] The maximal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @return {Promise} A promise resolving on pictures as an array.
	 */
	requestPictures(boundingBox, options) {
		let data = Object.assign({
			picsPerRequest: 100,
			page: 0,
			pictures: [],
			bbox: new LatLngBounds(boundingBox.getSouthWest().wrap(), boundingBox.getNorthEast().wrap())
		}, this.options, options);
		
		return this.download(data);
	}
	
	/**
	 * Get summary statistics about pictures available in the given area.
	 * To get summary when ready, suscribe to the {@link #fetcherdonesummary|donesummary} event.
	 * @param {LatLngBounds} boundingBox The bounding box of the area where you want to get the summary.
	 * @param {object} [options] The options for summary retrieval.
	 * @param {int} [options.mindate] The minimal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {int} [options.maxdate] The maximal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @return {Promise} A promise resolving on pictures summary
	 */
	requestSummary(boundingBox, options) {
		options = Object.assign({}, this.options, options);
		options.bbox = boundingBox;
		
		let url = API_URL+"list/nearby-photos/";
		let params = {
			"coordinate": boundingBox.getCenter().lat+","+boundingBox.getCenter().lng,
			"radius": boundingBox.getNorthEast().distanceTo(boundingBox.getSouthWest())/2
		};
		if(options.mindate) {
			params["date"] = (new Date(options.mindate)).toISOString().split('T')[0];
		}
		
		return this.ajaxPost(url, params, "json")
			.then(data => {
				if(data === null || data.status === undefined || data.status.apiCode !== "600") {
					this.fail(null, null, new Error("ctrl.fetcher.kartaview.getsummaryfailed"));
				}
				
				//Parse data
				if(data.currentPageItems && data.currentPageItems.length > 0) {
					//Search most recent picture date
					data.currentPageItems.sort((a,b) => { return b.timestamp - a.timestamp; });
					
					let amount = data.currentPageItems.length;
					let last = data.currentPageItems[0].timestamp*1000;
					
					//Remove recent pictures if needed
					if(options.maxdate && last > options.maxdate) {
						let i = 0;
						while(i < data.currentPageItems.length && last > options.maxdate) {
							i++;
							amount--;
							last = (i < data.currentPageItems.length) ? data.currentPageItems[i].timestamp*1000 : 0;
						}
					}
					
					return {
						last: last,
						amount: (amount < 1000) ? "e"+amount : ">1000",
						bbox: options.bbox.toBBoxString()
					};
				}
				else {
					return {
						amount: "e0",
						bbox: options.bbox.toBBoxString()
					};
				}
			});
	}
	
	/**
	 * Get a list of pictures URL according to given OSM tags.
	 * @param {Object} tags The OSM tags to interpret.
	 * @return {string[]} The list of pictures URL read from tags.
	 */
	tagsToPictures(tags) {
		const result = [];
		
		if(tags.image) {
			tags.image.split(';').forEach(img => {
				if(img.startsWith("http://") || img.startsWith("https://")) {
					result.push(img);
				}
			});
		}
		
		return result;
	}

	/**
	 * Retrieves pictures metadata for current bounding box, page and date.
	 * @private
	 */
	download(data) {
		let url = API_URL+"list/nearby-photos/";
		let params = {
			"coordinate": data.bbox.getCenter().lat+","+data.bbox.getCenter().lng,
			"radius": data.bbox.getNorthEast().distanceTo(data.bbox.getSouthWest())/2
		};
		
		if(data.mindate) {
			params["date"] = (new Date(data.mindate)).toISOString().split('T')[0];
		}
		
		return this.ajaxPost(url, params, "json")
			.then(result => {
				if(result === null || result.status === undefined || result.status.apiCode !== "600") {
					this.fail(null, null, new Error("ctrl.fetcher.kartaview.getpicturesfailed"));
				}
				
				//Parse data
				if(result.currentPageItems && result.currentPageItems.length > 0) {
					result.currentPageItems.sort((a,b) => { return parseInt(a.timestamp) - parseInt(b.timestamp); });
					for(let i=0; i < result.currentPageItems.length; i++) {
						let pic = result.currentPageItems[i];
						let coords = new LatLng(pic.lat, pic.lng);
						
						if(
							data.bbox.contains(coords)
							&& (!data.maxdate || pic.timestamp*1000 <= data.maxdate)
						) {
							const url = "https://"+pic.lth_name.replace(/^storage([0-9]+)\//, "storage$1.openstreetcam.org/");
							data.pictures.push(new Picture(
								url,
								pic.timestamp*1000,
								coords,
								this.name,
								pic.username,
								"CC By-SA 4.0",
								"https://kartaview.org/details/"+pic.sequence_id+"/"+pic.sequence_index,
								(pic.heading) ? ((parseFloat(pic.heading) < 0) ? (parseFloat(pic.heading) + 360) % 360 : parseFloat(pic.heading)) : null,
								{ image: url }
							));
						}
					}
				}
				
				return data.pictures;
			});
	}
}
