import Fetcher from "../Fetcher";
import LatLng from "../../model/LatLng";
import LatLngBounds from "../../model/LatLngBounds";
import Picture from "../../model/Picture";

/**
 * Wikimedia Commons fetcher. It doesn't need any credentials.
 */
export default class WikiCommons extends Fetcher {
//CONSTRUCTOR
	/**
	 * Class constructor
	 */
	constructor() {
		super();
	}

//ACCESSORS
	/**
	 * Get this provider name.
	 * @return {string} The provider human-readable name.
	 */
	get name() {
		return "Wikimedia Commons";
	}
	
	/**
	 * Get this provider logo URL.
	 * @return {string} The provider logo URL.
	 */
	get logoUrl() {
		return "https://commons.wikimedia.org/static/images/project-logos/commonswiki.png";
	}
	
	/**
	 * Get this provider homepage URL.
	 * @return {string} The provider homepage URL.
	 */
	get homepageUrl() {
		return "https://commons.wikimedia.org";
	}
	
	/**
	 * Get all the images in the given area for this provider.
	 * To get pictures when ready, suscribe to the {@link #fetcherdonepictures|donepictures} event.
	 * @param {LatLngBounds} boundingBox The bounding box of the area where you want to get pictures.
	 * @param {object} [options] The options for pictures retrieval.
	 * @param {int} [options.mindate] The minimal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {int} [options.maxdate] The maximal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @return {Promise} A promise resolving on pictures as an array.
	 */
	requestPictures(boundingBox, options) {
		let data = Object.assign({
			picsPerRequest: 100,
			continue: null,
			pictures: [],
			bbox: new LatLngBounds(boundingBox.getSouthWest().wrap(), boundingBox.getNorthEast().wrap())
		}, this.options, options);
		
		return this.download(data);
	}
	
	/**
	 * Get summary statistics about pictures available in the given area.
	 * To get summary when ready, suscribe to the {@link #fetcherdonesummary|donesummary} event.
	 * @param {LatLngBounds} boundingBox The bounding box of the area where you want to get the summary.
	 * @param {object} [options] The options for summary retrieval.
	 * @param {int} [options.mindate] The minimal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {int} [options.maxdate] The maximal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @return {Promise} A promise resolving on pictures summary
	 */
	requestSummary(boundingBox, options) {
		options = Object.assign({
			bbox: boundingBox
		}, this.options, options);
		
		let url = "https://commons.wikimedia.org/w/api.php?action=query&format=json&origin=*&prop=coordinates|imageinfo"
			+ "&generator=geosearch&iiprop=timestamp|user|url|extmetadata|metadata|size&iiextmetadatafilter=LicenseShortName"
			+ "&ggsbbox=" + boundingBox.getNorth() + "|" + boundingBox.getWest() + "|" + boundingBox.getSouth() + "|" + boundingBox.getEast()
			+ "&ggslimit=100"
			+ "&iilimit=99"
			+ "&colimit=99"
			+ "&ggsnamespace=6&&iimetadataversion=latest";
		
		return this.ajax(url, "json")
			.then(data => {
				//Parse data
				if(data.query) {
					if(data.query.pages && Object.keys(data.query.pages).length > 0) {
						let lastDate = null;
						let picAmount = 0;
						for(let picId in data.query.pages) {
							let pic = data.query.pages[picId];
							
							if(
								pic.imageinfo
								&& pic.imageinfo.length > 0
								&& pic.coordinates
								&& pic.coordinates.length > 0
								&& pic.imageinfo[0].url
								&& pic.imageinfo[0].timestamp
								&& pic.coordinates[0].lat
								&& pic.coordinates[0].lon
								&& pic.imageinfo[0].user
								&& pic.imageinfo[0].extmetadata.LicenseShortName
							) {
								//Try to find DateTime in metadata
								let date = null;
								if(pic.imageinfo[0].metadata && pic.imageinfo[0].metadata.length > 0) {
									let mdId = 0;
									while(mdId < pic.imageinfo[0].metadata.length && date == null) {
										if(pic.imageinfo[0].metadata[mdId].name == "DateTime") {
											let ts = pic.imageinfo[0].metadata[mdId].value;
											if(ts.match(/^[0-9]{4}:[0-9]{2}:[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/)) {
												ts = ts.replace(/^([0-9]{4}):([0-9]{2}):([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})$/, "$1-$2-$3T$4:$5:$6Z");
												date = (new Date(ts)).getTime();
											}
										}
										mdId++;
									}
								}
								if(date == null) { date = (new Date(pic.imageinfo[0].timestamp)).getTime(); }
								
								if(
									!isNaN(date)
									&& (options.mindate == null || date >= options.mindate)
									&& (options.maxdate == null || date <= options.maxdate)
								) {
									picAmount++;
									if(lastDate == null || lastDate <= date) { lastDate = date; }
								}
							}
						}
						if(picAmount > 0) {
							return {
								last: lastDate,
								amount: ">"+picAmount,
								bbox: options.bbox.toBBoxString()
							};
						}
						else {
							return {
								amount: "e0",
								bbox: options.bbox.toBBoxString()
							};
						}
					}
					else {
						return {
							amount: "e0",
							bbox: options.bbox.toBBoxString()
						};
					}
				}
				else if(data.batchcomplete === "") {
					return {
						amount: "e0",
						bbox: options.bbox.toBBoxString()
					};
				}
				else {
					throw new Error("ctrl.fetcher.wikicommons.getlightfailed");
				}
			});
	}
	
	/**
	 * Get a list of pictures URL according to given OSM tags.
	 * @param {Object} tags The OSM tags to interpret.
	 * @return {string[]} The list of pictures URL read from tags.
	 */
	tagsToPictures(tags) {
		const result = [];
		
		if(tags.wikimedia_commons) {
			tags.wikimedia_commons.split(";").forEach(pic => {
				if(pic.startsWith("File:")) {
					const wmid = pic.substring(5).replace(/ /g, '_');
					const digest = this._md5(wmid);
					const url = 'https://upload.wikimedia.org/wikipedia/commons/' + digest[0] + '/' + digest[0] + digest[1] + '/' + encodeURIComponent(wmid);
					result.push(this.toThumbURL(url));
				}
			});
		}
		
		return result;
	}


//OTHER METHODS
	/**
	 * Retrieves pictures metadata for current bounding box, page and date.
	 * @private
	 */
	download(info) {
		let url = "https://commons.wikimedia.org/w/api.php?action=query&format=json&origin=*&prop=coordinates|imageinfo"
			+ "&continue=" + ((info.continue && info.continue.continue) ? info.continue.continue : "")
			+ ((info.continue && info.continue.cocontinue) ? "&cocontinue=" + info.continue.cocontinue : "")
			+ "&generator=geosearch&iiprop=timestamp|user|url|extmetadata|metadata|size&iiextmetadatafilter=LicenseShortName"
			+ "&ggsbbox=" + info.bbox.getNorth() + "|" + info.bbox.getWest() + "|" + info.bbox.getSouth() + "|" + info.bbox.getEast()
			+ "&ggslimit=" + info.picsPerRequest
			+ "&iilimit=" + (info.picsPerRequest-1)
			+ "&colimit=" + (info.picsPerRequest-1)
			+ "&ggsnamespace=6&&iimetadataversion=latest";
		
		return this.ajax(url, "json")
			.then(result => {
				//Parse data
				if(result.query && result.query.pages && Object.keys(result.query.pages).length > 0) {
					for(let picId in result.query.pages) {
						let pic = result.query.pages[picId];
						
						if(
							pic.imageinfo
							&& pic.imageinfo.length > 0
							&& pic.coordinates
							&& pic.coordinates.length > 0
							&& pic.imageinfo[0].url
							&& pic.imageinfo[0].timestamp
							&& pic.coordinates[0].lat
							&& pic.coordinates[0].lon
							&& pic.imageinfo[0].user
							&& pic.imageinfo[0].extmetadata.LicenseShortName
						) {
							//Try to find DateTime in metadata
							let date = null;
							if(pic.imageinfo[0].metadata && pic.imageinfo[0].metadata.length > 0) {
								let mdId = 0;
								while(mdId < pic.imageinfo[0].metadata.length && date == null) {
									if(pic.imageinfo[0].metadata[mdId].name == "DateTime") {
										let ts = pic.imageinfo[0].metadata[mdId].value;
										if(ts.match(/^[0-9]{4}:[0-9]{2}:[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/)) {
											ts = ts.replace(/^([0-9]{4}):([0-9]{2}):([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})$/, "$1-$2-$3T$4:$5:$6Z");
											date = (new Date(ts)).getTime();
										}
									}
									mdId++;
								}
							}
							
							if(date == null) { date = (new Date(pic.imageinfo[0].timestamp)).getTime(); }
							
							if(
								!isNaN(date)
								&& (info.mindate == null || date >= info.mindate)
								&& (info.maxdate == null || date <= info.maxdate)
							) {
								info.pictures.push(new Picture(
									(pic.imageinfo[0].width && pic.imageinfo[0].width > 1024) ? this.toThumbURL(pic.imageinfo[0].url) : pic.imageinfo[0].url,
									date,
									new LatLng(pic.coordinates[0].lat, pic.coordinates[0].lon),
									this.name,
									pic.imageinfo[0].user,
									pic.imageinfo[0].extmetadata.LicenseShortName.value,
									pic.imageinfo[0].descriptionurl,
									null,
									{ wikimedia_commons: pic.title },
									this.toThumbURL(pic.imageinfo[0].url, 640)
								));
							}
						}
					}
					
					info.continue = (result.continue && (result.continue.cocontinue || result.continue.continue)) ? result.continue : null;
				}
				else {
					info.continue = null;
				}
				
				//Next step
				if(info.continue !== null) {
					return this.download(info);
				}
				else {
					return info.pictures;
				}
			});
	}
	
	/**
	 * Convert an URL of a WikiCommons image into its thumbnail
	 * @param url The image URL
	 * @param size The image size in pixels
	 * @return The thumbnail URL
	 * @private
	 */
	toThumbURL(url, size) {
		size = size || 1024;
		let rgx = /^(.+wikipedia\/commons)\/([a-zA-Z0-9]\/[a-zA-Z0-9]{2})\/(.+)$/;
		let rgxRes = rgx.exec(url);
		return rgxRes[1] + "/thumb/" + rgxRes[2] + "/" + rgxRes[3] + "/" + size + "px-" + rgxRes[3];
	}
	
	_md5(str) {
		let xl;
		
		let rotateLeft = function(lValue, iShiftBits) {
			return (lValue << iShiftBits) | (lValue >>> (32 - iShiftBits));
		};
		
		let addUnsigned = function(lX, lY) {
			let lX4, lY4, lX8, lY8, lResult;
			lX8 = (lX & 0x80000000);
			lY8 = (lY & 0x80000000);
			lX4 = (lX & 0x40000000);
			lY4 = (lY & 0x40000000);
			lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);
			if (lX4 & lY4) {
				return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
			}
			if (lX4 | lY4) {
				if (lResult & 0x40000000) {
					return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
				} else {
					return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
				}
			} else {
				return (lResult ^ lX8 ^ lY8);
			}
		};
		
		let _F = function(x, y, z) {
			return (x & y) | ((~x) & z);
		};
		let _G = function(x, y, z) {
			return (x & z) | (y & (~z));
		};
		let _H = function(x, y, z) {
			return (x ^ y ^ z);
		};
		let _I = function(x, y, z) {
			return (y ^ (x | (~z)));
		};
		
		let _FF = function(a, b, c, d, x, s, ac) {
			a = addUnsigned(a, addUnsigned(addUnsigned(_F(b, c, d), x), ac));
			return addUnsigned(rotateLeft(a, s), b);
		};
		
		let _GG = function(a, b, c, d, x, s, ac) {
			a = addUnsigned(a, addUnsigned(addUnsigned(_G(b, c, d), x), ac));
			return addUnsigned(rotateLeft(a, s), b);
		};
		
		let _HH = function(a, b, c, d, x, s, ac) {
			a = addUnsigned(a, addUnsigned(addUnsigned(_H(b, c, d), x), ac));
			return addUnsigned(rotateLeft(a, s), b);
		};
		
		let _II = function(a, b, c, d, x, s, ac) {
			a = addUnsigned(a, addUnsigned(addUnsigned(_I(b, c, d), x), ac));
			return addUnsigned(rotateLeft(a, s), b);
		};
		
		let convertToWordArray = function(str) {
			let lWordCount;
			let lMessageLength = str.length;
			let lNumberOfWords_temp1 = lMessageLength + 8;
			let lNumberOfWords_temp2 = (lNumberOfWords_temp1 - (lNumberOfWords_temp1 % 64)) / 64;
			let lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
			let lWordArray = new Array(lNumberOfWords - 1);
			let lBytePosition = 0;
			let lByteCount = 0;
			while (lByteCount < lMessageLength) {
				lWordCount = (lByteCount - (lByteCount % 4)) / 4;
				lBytePosition = (lByteCount % 4) * 8;
				lWordArray[lWordCount] = (lWordArray[lWordCount] | (str.charCodeAt(lByteCount) << lBytePosition));
				lByteCount++;
			}
			lWordCount = (lByteCount - (lByteCount % 4)) / 4;
			lBytePosition = (lByteCount % 4) * 8;
			lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80 << lBytePosition);
			lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
			lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
			return lWordArray;
		};
		
		let wordToHex = function(lValue) {
			let wordToHexValue = '',
			wordToHexValue_temp = '',
			lByte, lCount;
			for (lCount = 0; lCount <= 3; lCount++) {
				lByte = (lValue >>> (lCount * 8)) & 255;
				wordToHexValue_temp = '0' + lByte.toString(16);
				wordToHexValue = wordToHexValue + wordToHexValue_temp.substr(wordToHexValue_temp.length - 2, 2);
			}
			return wordToHexValue;
		};
		
		let x = [],
		k, AA, BB, CC, DD, a, b, c, d, S11 = 7,
		S12 = 12,
		S13 = 17,
		S14 = 22,
		S21 = 5,
		S22 = 9,
		S23 = 14,
		S24 = 20,
		S31 = 4,
		S32 = 11,
		S33 = 16,
		S34 = 23,
		S41 = 6,
		S42 = 10,
		S43 = 15,
		S44 = 21;
		
		str = this._utf8_encode(str);
		x = convertToWordArray(str);
		a = 0x67452301;
		b = 0xEFCDAB89;
		c = 0x98BADCFE;
		d = 0x10325476;
		
		xl = x.length;
		for (k = 0; k < xl; k += 16) {
			AA = a;
			BB = b;
			CC = c;
			DD = d;
			a = _FF(a, b, c, d, x[k + 0], S11, 0xD76AA478);
			d = _FF(d, a, b, c, x[k + 1], S12, 0xE8C7B756);
			c = _FF(c, d, a, b, x[k + 2], S13, 0x242070DB);
			b = _FF(b, c, d, a, x[k + 3], S14, 0xC1BDCEEE);
			a = _FF(a, b, c, d, x[k + 4], S11, 0xF57C0FAF);
			d = _FF(d, a, b, c, x[k + 5], S12, 0x4787C62A);
			c = _FF(c, d, a, b, x[k + 6], S13, 0xA8304613);
			b = _FF(b, c, d, a, x[k + 7], S14, 0xFD469501);
			a = _FF(a, b, c, d, x[k + 8], S11, 0x698098D8);
			d = _FF(d, a, b, c, x[k + 9], S12, 0x8B44F7AF);
			c = _FF(c, d, a, b, x[k + 10], S13, 0xFFFF5BB1);
			b = _FF(b, c, d, a, x[k + 11], S14, 0x895CD7BE);
			a = _FF(a, b, c, d, x[k + 12], S11, 0x6B901122);
			d = _FF(d, a, b, c, x[k + 13], S12, 0xFD987193);
			c = _FF(c, d, a, b, x[k + 14], S13, 0xA679438E);
			b = _FF(b, c, d, a, x[k + 15], S14, 0x49B40821);
			a = _GG(a, b, c, d, x[k + 1], S21, 0xF61E2562);
			d = _GG(d, a, b, c, x[k + 6], S22, 0xC040B340);
			c = _GG(c, d, a, b, x[k + 11], S23, 0x265E5A51);
			b = _GG(b, c, d, a, x[k + 0], S24, 0xE9B6C7AA);
			a = _GG(a, b, c, d, x[k + 5], S21, 0xD62F105D);
			d = _GG(d, a, b, c, x[k + 10], S22, 0x2441453);
			c = _GG(c, d, a, b, x[k + 15], S23, 0xD8A1E681);
			b = _GG(b, c, d, a, x[k + 4], S24, 0xE7D3FBC8);
			a = _GG(a, b, c, d, x[k + 9], S21, 0x21E1CDE6);
			d = _GG(d, a, b, c, x[k + 14], S22, 0xC33707D6);
			c = _GG(c, d, a, b, x[k + 3], S23, 0xF4D50D87);
			b = _GG(b, c, d, a, x[k + 8], S24, 0x455A14ED);
			a = _GG(a, b, c, d, x[k + 13], S21, 0xA9E3E905);
			d = _GG(d, a, b, c, x[k + 2], S22, 0xFCEFA3F8);
			c = _GG(c, d, a, b, x[k + 7], S23, 0x676F02D9);
			b = _GG(b, c, d, a, x[k + 12], S24, 0x8D2A4C8A);
			a = _HH(a, b, c, d, x[k + 5], S31, 0xFFFA3942);
			d = _HH(d, a, b, c, x[k + 8], S32, 0x8771F681);
			c = _HH(c, d, a, b, x[k + 11], S33, 0x6D9D6122);
			b = _HH(b, c, d, a, x[k + 14], S34, 0xFDE5380C);
			a = _HH(a, b, c, d, x[k + 1], S31, 0xA4BEEA44);
			d = _HH(d, a, b, c, x[k + 4], S32, 0x4BDECFA9);
			c = _HH(c, d, a, b, x[k + 7], S33, 0xF6BB4B60);
			b = _HH(b, c, d, a, x[k + 10], S34, 0xBEBFBC70);
			a = _HH(a, b, c, d, x[k + 13], S31, 0x289B7EC6);
			d = _HH(d, a, b, c, x[k + 0], S32, 0xEAA127FA);
			c = _HH(c, d, a, b, x[k + 3], S33, 0xD4EF3085);
			b = _HH(b, c, d, a, x[k + 6], S34, 0x4881D05);
			a = _HH(a, b, c, d, x[k + 9], S31, 0xD9D4D039);
			d = _HH(d, a, b, c, x[k + 12], S32, 0xE6DB99E5);
			c = _HH(c, d, a, b, x[k + 15], S33, 0x1FA27CF8);
			b = _HH(b, c, d, a, x[k + 2], S34, 0xC4AC5665);
			a = _II(a, b, c, d, x[k + 0], S41, 0xF4292244);
			d = _II(d, a, b, c, x[k + 7], S42, 0x432AFF97);
			c = _II(c, d, a, b, x[k + 14], S43, 0xAB9423A7);
			b = _II(b, c, d, a, x[k + 5], S44, 0xFC93A039);
			a = _II(a, b, c, d, x[k + 12], S41, 0x655B59C3);
			d = _II(d, a, b, c, x[k + 3], S42, 0x8F0CCC92);
			c = _II(c, d, a, b, x[k + 10], S43, 0xFFEFF47D);
			b = _II(b, c, d, a, x[k + 1], S44, 0x85845DD1);
			a = _II(a, b, c, d, x[k + 8], S41, 0x6FA87E4F);
			d = _II(d, a, b, c, x[k + 15], S42, 0xFE2CE6E0);
			c = _II(c, d, a, b, x[k + 6], S43, 0xA3014314);
			b = _II(b, c, d, a, x[k + 13], S44, 0x4E0811A1);
			a = _II(a, b, c, d, x[k + 4], S41, 0xF7537E82);
			d = _II(d, a, b, c, x[k + 11], S42, 0xBD3AF235);
			c = _II(c, d, a, b, x[k + 2], S43, 0x2AD7D2BB);
			b = _II(b, c, d, a, x[k + 9], S44, 0xEB86D391);
			a = addUnsigned(a, AA);
			b = addUnsigned(b, BB);
			c = addUnsigned(c, CC);
			d = addUnsigned(d, DD);
		}
		
		let temp = wordToHex(a) + wordToHex(b) + wordToHex(c) + wordToHex(d);
		
		return temp.toLowerCase();
	}
	
	_utf8_encode(argString) {
		if (argString === null || typeof argString === 'undefined') {
			return '';
		}
		
		let string = (argString + ''); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
		let utftext = '',
		start, end, stringl = 0;
		
		start = end = 0;
		stringl = string.length;
		for (let n = 0; n < stringl; n++) {
			let c1 = string.charCodeAt(n);
			let enc = null;
			
			if (c1 < 128) {
				end++;
			} else if (c1 > 127 && c1 < 2048) {
				enc = String.fromCharCode(
					(c1 >> 6) | 192, (c1 & 63) | 128
				);
			} else if ((c1 & 0xF800) != 0xD800) {
				enc = String.fromCharCode(
					(c1 >> 12) | 224, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
				);
			} else { // surrogate pairs
				if ((c1 & 0xFC00) != 0xD800) {
					throw new RangeError('Unmatched trail surrogate at ' + n);
				}
				let c2 = string.charCodeAt(++n);
				if ((c2 & 0xFC00) != 0xDC00) {
					throw new RangeError('Unmatched lead surrogate at ' + (n - 1));
				}
				c1 = ((c1 & 0x3FF) << 10) + (c2 & 0x3FF) + 0x10000;
				enc = String.fromCharCode(
					(c1 >> 18) | 240, ((c1 >> 12) & 63) | 128, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
				);
			}
			if (enc !== null) {
				if (end > start) {
					utftext += string.slice(start, end);
				}
				utftext += enc;
				start = end = n + 1;
			}
		}
		
		if (end > start) {
			utftext += string.slice(start, stringl);
		}
		
		return utftext;
	}
}
