import Fetcher from "../Fetcher";
import LatLng from "../../model/LatLng";
import LatLngBounds from "../../model/LatLngBounds";
import Picture from "../../model/Picture";

const API_URL = "https://api.panoramax.xyz/api";

/**
 * Panoramax fetcher. It doesn't need any credential.
 */
export default class Panoramax extends Fetcher {
//CONSTRUCTOR
	/**
	 * Class constructor.
	 */
	constructor() {
		super();
	}

//ACCESSORS
	/**
	 * Get this provider name.
	 * @return {string} The provider human-readable name.
	 */
	get name() {
		return "Panoramax";
	}
	
	/**
	 * Get this provider logo URL.
	 * @return {string} The provider logo URL.
	 */
	get logoUrl() {
		return "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Panoramax.svg/600px-Panoramax.svg.png";
	}
	
	/**
	 * Get this provider homepage URL.
	 * @return {string} The provider homepage URL.
	 */
	get homepageUrl() {
		return "https://panoramax.fr/";
	}

//OTHER METHODS
	/**
	 * Get all the images in the given area for this provider.
	 * To get pictures when ready, suscribe to the {@link #fetcherdonepictures|donepictures} event.
	 * @param {LatLngBounds} boundingBox The bounding box of the area where you want to get pictures.
	 * @param {object} [options] The options for pictures retrieval.
	 * @param {int} [options.mindate] The minimal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {int} [options.maxdate] The maximal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @return {Promise} A promise resolving on pictures as an array.
	 */
	requestPictures(boundingBox, options) {
		let data = Object.assign({
			pictures: [],
			bbox: new LatLngBounds(boundingBox.getSouthWest().wrap(), boundingBox.getNorthEast().wrap())
		}, this.options, options);
		
		return this.download(data);
	}
	
	/**
	 * Get summary statistics about pictures available in the given area.
	 * To get summary when ready, suscribe to the {@link #fetcherdonesummary|donesummary} event.
	 * @param {LatLngBounds} boundingBox The bounding box of the area where you want to get the summary.
	 * @param {object} [options] The options for summary retrieval.
	 * @param {int} [options.mindate] The minimal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {int} [options.maxdate] The maximal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @return {Promise} A promise resolving on pictures summary
	 */
	requestSummary(boundingBox, options) {
		options = Object.assign({}, this.options, options);
		options.bbox = boundingBox;
		let data = Object.assign({
			pictures: [],
		}, this.options, options);
		
		return this.download(data).then(pictures => {
			if(pictures.length > 0) {
				return {
					last: pictures[pictures.length-1].date,
					amount: (pictures.length < 10000) ? "e"+pictures.length : ">10000",
					bbox: options.bbox.toBBoxString()
				};
			}
			else {
				return {
					amount: "e0",
					bbox: options.bbox.toBBoxString()
				};
			}
		});
	}
	
	/**
	 * Get a list of pictures URL according to given OSM tags.
	 * @param {Object} tags The OSM tags to interpret.
	 * @return {string[]} The list of pictures URL read from tags.
	 */
	tagsToPictures(tags) {
		const result = [];
		
		Object.keys(tags).filter(k => k.startsWith("panoramax")).forEach(k => {
			tags[k].split(';').forEach(img => {
				if(img.startsWith("http://") || img.startsWith("https://")) {
					result.push(img);
				}
				else {
					result.push(API_URL+"/pictures/"+img+"/hd.jpg")
				}
			});
		})
		
		return result;
	}

	/**
	 * Retrieves pictures metadata for current bounding box, page and date.
	 * @private
	 */
	download(data) {
		let url = API_URL+"/search"
			+ "?bbox=" + data.bbox.toBBoxString()
			+ "&limit=10000";
		
		return this.ajax(url, "json")
			.then(result => {
				if(result === null || result.error != undefined) {
					this.fail(null, null, new Error("ctrl.fetcher.panoramax.getpicturesfailed"));
				}
				
				//Parse data
				if(result.features && result.features.length > 0) {
					result.features.sort((a,b) => { return a.properties.datetime.localeCompare(b.properties.datetime); });
					for(let i=0; i < result.features.length; i++) {
						let pic = result.features[i];
						let dt = new Date(pic.properties.datetime);
						let coords = new LatLng(pic.geometry.coordinates[1], pic.geometry.coordinates[0]);
						
						if(
							(!data.mindate || dt.getTime() >= data.mindate)
							&& (!data.maxdate || dt.getTime() <= data.maxdate)
						) {
							data.pictures.push(new Picture(
								pic.assets.hd.href,
								dt.getTime(),
								coords,
								this.name,
								pic.providers[0].name,
								pic.properties.license,
								pic.links.find(l => l.rel === "self").href,
								pic.properties["view:azimuth"],
								{ panoramax: pic.id },
								pic.assets.thumb.href,
							));
						}
					}
				}
				
				return data.pictures;
			});
	}
}
