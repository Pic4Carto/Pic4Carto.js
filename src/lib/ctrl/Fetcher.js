/**
 * A fetcher is an object which will download all pictures of a given provider, in a given area.
 * This is an abstract class, it must be extended and the methods overridden.
 * @private
 */
export default class Fetcher {
//CONSTRUCTOR
	constructor() {
		if(this.constructor.name === "Fetcher") {
			throw new TypeError("ctrl.fetcher.cantinstantiate");
		}

		if(this.requestPictures === undefined) {
			throw new TypeError("ctrl.fetcher.mustoverride.requestPictures");
		}

		if(this.requestSummary === undefined) {
			throw new TypeError("ctrl.fetcher.mustoverride.requestSummary");
		}

		if(this.tagsToPictures === undefined) {
			throw new TypeError("ctrl.fetcher.mustoverride.tagsToPictures");
		}

		if(this.name === undefined) {
			throw new TypeError("ctrl.fetcher.mustoverride.getName");
		}

		if(this.logoUrl === undefined) {
			throw new TypeError("ctrl.fetcher.mustoverride.getLogoUrl");
		}

		if(this.homepageUrl === undefined) {
			throw new TypeError("ctrl.fetcher.mustoverride.getHomepageUrl");
		}

		this.options = {
			mindate: null,
			maxdate: null
		};
	}

//ACCESSORS
	/**
	 * Get this provider name.
	 * @return {string} The provider human-readable name.
	 * @abstract
	 */
	get name() {
		throw new TypeError("ctrl.fetcher.mustoverride.getName");
	}

//OTHER METHODS
	/**
	 * Get all the images in the given area for this provider.
	 * To get pictures when ready, suscribe to the {@link #fetcherdonepictures|donepictures} event.
	 * @param {LatLngBounds} boundingBox The bounding box of the area where you want to get pictures.
	 * @param {object} [options] The options for pictures retrieval.
	 * @param {int} [options.mindate] The minimal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {int} [options.maxdate] The maximal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @return {Promise} A promise resolving on pictures as an array.
	 * @abstract
	 */
	requestPictures(boundingBox, options) {
		throw new TypeError("ctrl.fetcher.mustoverride.requestPictures");
	}

	/**
	 * Get summary statistics about pictures available in the given area.
	 * To get summary when ready, suscribe to the {@link #fetcherdonesummary|donesummary} event.
	 * @param {LatLngBounds} boundingBox The bounding box of the area where you want to get the summary.
	 * @param {object} [options] The options for summary retrieval.
	 * @param {int} [options.mindate] The minimal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {int} [options.maxdate] The maximal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @return {Promise} A promise resolving on pictures summary
	 * @abstract
	 */
	requestSummary(boundingBox, options) {
		throw new TypeError("ctrl.fetcher.mustoverride.requestSummary");
	}

	/**
	 * Get feature detections in the given area.
	 * To get detections when ready, suscribe to the {@link #fetcherdonedetections|donedetections} event.
	 * @param {LatLngBounds} boundingBox The bounding box of the area where you want to get the detections.
	 * @param {object} [options] The options for detections retrieval.
	 * @param {int[]} [options.types] The list of detections types to look for (use Detection.* constants).
	 * @return {Promise} A promise resolving on detections
	 * @abstract
	 */
	requestDetections(boundingBox, options) {
		throw new TypeError("ctrl.fetcher.mustoverride.requestDetections");
	}

	/**
	 * Get a list of pictures URL according to given OSM tags.
	 * @param {Object} tags The OSM tags to interpret.
	 * @return {string[]} The list of pictures URL read from tags.
	 * @abstract
	 */
	tagsToPictures(tags) {
		throw new TypeError("ctrl.fetcher.mustoverride.tagsToPictures");
	}

	/**
	 * Creates an Ajax GET request.
	 * @param {string} url The URL of the data to retrieve.
	 * @param {string} type The expected file format.
	 * @return {Promise} A promise, solving on downloaded data, and rejecting on errors
	 */
	async ajax(url, type) {
		return fetch(url).then(async res => {
			if(type == "json") {
				return await res.json();
			}
			else if(type == "json_h") {
				return { json: await res.json(), xmlhttp: res };
			}
			else {
				return await res.text();
			}
		});
	}

	/**
	 * Creates an Ajax POST request.
	 * @param {string} url The URL of the data to retrieve.
	 * @param {object} [parameters] The HTTP parameters.
	 * @param {string} responseType The expected file format.
	 * @return {Promise} A promise, solving on downloaded data, and rejecting on errors
	 */
	async ajaxPost(url, parameters, responseType) {
		//Convert parameters object into a string of HTTP parameters
		let body;
		if(parameters) {
			body = Object.entries(parameters).map(([k,v]) => `${k}=${encodeURIComponent(v)}`).join("&");
		}

		return fetch(url, {
			method: "post",
			body,
			headers: {
				"Content-Type": "application/x-www-form-urlencoded"
			}
		}).then(async res => {
			if(responseType == "json") {
				return await res.json();
			}
			else {
				return await res.text();
			}
		});
	}
}
