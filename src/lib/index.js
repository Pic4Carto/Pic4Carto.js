/*
 * This file is part of Pic4Carto.js.
 * 
 * Pic4Carto.js is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto.js is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.js.  If not, see <http://www.gnu.org/licenses/>.
 */

import Detection from "./model/Detection";
import LatLng from "./model/LatLng";
import LatLngBounds from "./model/LatLngBounds";
import Picture from "./model/Picture";

import PicturesManager from "./ctrl/PicturesManager";
import EventLauncher from "./ctrl/EventLauncher";
import Fetcher from "./ctrl/Fetcher";

import CSV from "./ctrl/fetchers/CSV";
import Flickr from "./ctrl/fetchers/Flickr";
import KartaView from "./ctrl/fetchers/KartaView";
import Mapillary from "./ctrl/fetchers/Mapillary";
import Panoramax from "./ctrl/fetchers/Panoramax";
import WikiCommons from "./ctrl/fetchers/WikiCommons";

const fetchers = {
  CSV,
  Flickr,
  KartaView,
  Mapillary,
  Panoramax,
  WikiCommons,
};

export {
  PicturesManager,
  EventLauncher,
  Fetcher,
  fetchers,
  Detection,
  LatLng,
  LatLngBounds,
  Picture
};
