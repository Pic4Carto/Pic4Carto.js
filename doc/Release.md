# Make a release

GeoVisio uses [semantic versioning](https://semver.org/) for its release numbers.

Run these commands in order to issue a new release:

```bash
git checkout develop

vim package.json		# Change version
vim package-lock.json	# Change version
npm run doc

vim CHANGELOG.md	# Replace unreleased to version number (and links at bottom)

git add *
git commit -m "Release vx.x.x"
git tag -a vx.x.x -m "Release vx.x.x"
git push origin develop
git checkout main
git merge develop
git push origin main --tags
```
